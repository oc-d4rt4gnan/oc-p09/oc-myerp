package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Classe de test pour le Bean {@link JournalComptable}
 */
@Tag("Journal_Comptable_Test")
@DisplayName("Tests pour le Bean JournalComptable")
public class JournalComptableTest {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Instance d'une liste de journal comptable
    private List<JournalComptable> vListJournalComptable;



    /*
     * =================================================================================================================
     *                                              Methodes d'initialisation
     * =================================================================================================================
     */

    @BeforeEach
    public void initList() {
        vListJournalComptable = new ArrayList<>();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes de test
     * =================================================================================================================
     */

    @Test
    @DisplayName(
            "Si on fournit une liste de JournalComptable, " +
            "lorsqu'on veut en recuperer un par son code, " +
            "alors on obtient le bon journal.")
    public void givenAListOfJournalComptable_whenGetByCode_thenReturnTheRightJournalComptable() {
        // GIVEN
        vListJournalComptable.add(new JournalComptable("PI19C5", "Un libelle"));
        vListJournalComptable.add(new JournalComptable("1DM2Y5", "Un deuxième libelle"));
        vListJournalComptable.add(new JournalComptable("IBKIAB", "Un troisième libelle"));
        vListJournalComptable.add(new JournalComptable("045J0W", "Un quatrième libelle"));

        // WHEN
        JournalComptable actualResult = JournalComptable.getByCode(vListJournalComptable, "045J0W");

        // THEN
        assertThat(actualResult.getLibelle()).isEqualTo("Un quatrième libelle");
    }


    @Test
    @DisplayName(
            "Si on fournit un code n'existant pas dans la liste de JournalComptable, " +
            "lorsqu'on veut en recuperer un par son code, " +
            "alors on obtient un objet null.")
    public void givenAListOfJournalComptableAndAWrongCode_whenGetByCode_thenReturnNull() {
        // GIVEN
        vListJournalComptable.add(new JournalComptable("GEX525", "Un libelle"));
        vListJournalComptable.add(new JournalComptable("2SQVGG", "Un deuxième libelle"));
        vListJournalComptable.add(new JournalComptable("55KQ5A", "Un troisième libelle"));
        vListJournalComptable.add(new JournalComptable("7ZTH5B", "Un quatrième libelle"));

        // WHEN
        JournalComptable actualResult = JournalComptable.getByCode(vListJournalComptable, "045J0W");

        // THEN
        assertThat(actualResult).isNull();
    }


    @Test
    @DisplayName(
            "Si on fournit une liste de JournalComptable vide, " +
            "lorsqu'on veut en recuperer un par son code, " +
            "alors on obtient un objet null.")
    public void givenAnEmptyListOfJournalComptable_whenGetByCode_thenReturnNull() {
        // GIVEN -- Nothing to do

        // WHEN
        JournalComptable actualResult = JournalComptable.getByCode(vListJournalComptable, "045J0W");

        // THEN
        assertThat(actualResult).isNull();
    }
}
