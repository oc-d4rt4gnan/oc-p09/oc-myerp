package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Classe de test pour le Bean {@link EcritureComptable}
 */
@Tag("Ecriture_Comptable_Test")
@DisplayName("Tests pour le Bean EcritureComptable.")
class EcritureComptableTest {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Instance d'une ecriture comptable
    private EcritureComptable vEcriture;



    /*
     * =================================================================================================================
     *                                              Methodes d'initialisation
     * =================================================================================================================
     */

    @BeforeEach
    void initEcriture() {
        vEcriture = new EcritureComptable();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes de test
     * =================================================================================================================
     */

    @Nested
    @Tag("Balance_Tests")
    @DisplayName("Doit pouvoir verifier l'equilibre entre les montants au debit et au credit.")
    class BalanceTests {
        @Test
        @DisplayName(
                "Si on fournit 2 valeurs en BigDecimal identique, " +
                "lorsqu'on verifie l'equilibre des montants, " +
                "alors on recupere une valeur TRUE.")
        void givenTwoIdenticalBigDecimal_whenIsEquilibree_thenReturnTrue() {
            // GIVEN
            BigDecimal totalDebit = new BigDecimal(301);
            BigDecimal totalCredit = new BigDecimal(301);

            // WHEN
            boolean actualResult = vEcriture.isEquilibree(totalDebit, totalCredit);

            // THEN
            assertThat(actualResult).isTrue();
        }


        @Test
        @DisplayName(
                "Si on fournit 2 valeurs en BigDecimal differentes, " +
                "lorsqu'on verifie l'equilibre des montants, " +
                "alors on recupere une valeur FALSE.")
        void givenTwoDifferentBigDecimal_whenIsEquilibree_thenReturnFalse() {
            // GIVEN
            BigDecimal totalDebit = new BigDecimal(31);
            BigDecimal totalCredit = new BigDecimal(30);

            // WHEN
            boolean actualResult = vEcriture.isEquilibree(totalDebit, totalCredit);

            // THEN
            assertThat(actualResult).isFalse();
        }


        @ParameterizedTest(name = "Le resultat de l'equilibre entre {0} et {1} doit etre {2}")
        @CsvSource({ "null,31,false", "2,null,false", "null,null,true" })
        @DisplayName(
                "Si on fournit une valeur null, " +
                "lorsqu'on verifie l'equilibre des montants, " +
                "on obtient quand meme un resultat.")
        void givenNullValue_whenIsEquilibree_thenReturnAnAnswer(
                String totalDebit, String totalCredit, boolean expectedResult) {
            // GIVEN
            BigDecimal totalDebitBD = totalDebit.equals("null") ? null : new BigDecimal(totalDebit);
            BigDecimal totalCreditBD = totalCredit.equals("null") ? null : new BigDecimal(totalCredit);

            // WHEN
            boolean actualResult = vEcriture.isEquilibree(totalDebitBD, totalCreditBD);

            // THEN
            assertThat(actualResult).isEqualTo(expectedResult);
        }
    }


    @Nested
    @Tag("Total_Debit_Tests")
    @DisplayName("Doit toujours retourner la somme des montants au debit.")
    class TotalDebitTests {
        @Test
        @DisplayName(
                "Si on fournit une liste de LigneEcriture, " +
                "lorsqu'on calcule le montant total des lignes au debit, " +
                "on obtient le bon resultat.")
        void givenListLigneEcriture_whenGetTotalDebit_thenReturnTheSumOfTheDebitAmounts() {
            // GIVEN
            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "200.50", new BigDecimal(200.50), BigDecimal.ZERO
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "67.50", new BigDecimal(100.50), new BigDecimal(33)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "-301", BigDecimal.ZERO, new BigDecimal(301)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "33", new BigDecimal(40), new BigDecimal(7)
                    )
            );

            // WHEN
            BigDecimal totalDebit = vEcriture.getTotalDebit();

            // THEN
            BigDecimal expectedResult = new BigDecimal(341);
            assertThat(totalDebit).isEqualByComparingTo(expectedResult);
        }


        @Test
        @DisplayName(
                "Si on fournit un montant null au debit, " +
                "lorsqu'on calcule le montant total des lignes au debit, " +
                "on obtient le bon resultat.")
        void givenListLigneEcritureWithNullValue_whenGetTotalDebit_thenReturnTheSumOfTheDebitAmounts() {
            // GIVEN
            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "62.47", new BigDecimal(62.47), BigDecimal.ZERO
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "60.25", new BigDecimal(88.25), new BigDecimal(28)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "-78", null, new BigDecimal(78)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "-40", new BigDecimal(11), new BigDecimal(51)
                    )
            );

            // WHEN
            BigDecimal totalDebit = vEcriture.getTotalDebit();

            // THEN
            BigDecimal expectedResult = new BigDecimal(161.72);
            assertThat(totalDebit).isEqualByComparingTo(expectedResult);
        }
    }


    @Nested
    @Tag("Total_Credit_Tests")
    @DisplayName("Doit toujours retourner la somme des montants au credit.")
    class TotalCreditTests {
        @Test
        @DisplayName(
                "Si on fournit une liste de LigneEcriture, " +
                "lorsqu'on calcule le montant total des lignes au credit, " +
                "on obtient le bon resultat.")
        void givenListLigneEcriture_whenGetTotalCredit_thenReturnTheSumOfTheCreditAmounts() {
            // GIVEN
            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "99.38", new BigDecimal(99.36), BigDecimal.ZERO
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "11.26", new BigDecimal(70.26), new BigDecimal(59)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "-291", BigDecimal.ZERO, new BigDecimal(291)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "6", new BigDecimal(44), new BigDecimal(38)
                    )
            );

            // WHEN
            BigDecimal totalCredit = vEcriture.getTotalCredit();

            // THEN
            BigDecimal expectedResult = new BigDecimal(388);
            assertThat(totalCredit).isEqualByComparingTo(expectedResult);
        }


        @Test
        @DisplayName(
                "Si on fournit un montant null au credit, " +
                "lorsqu'on calcule le montant total des lignes au credit, " +
                "on obtient le bon resultat.")
        void givenListLigneEcritureWithNullValue_whenGetTotalCredit_thenReturnTheSumOfTheCreditAmounts() {
            // GIVEN
            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "46.49", new BigDecimal(46.49), null
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), "26.56", new BigDecimal(99.56), new BigDecimal(73)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "-95", BigDecimal.ZERO, new BigDecimal(95)
                    )
            );

            vEcriture.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), "48", new BigDecimal(86), new BigDecimal(38)
                    )
            );

            // WHEN
            BigDecimal totalCredit = vEcriture.getTotalCredit();

            // THEN
            BigDecimal expectedResult = new BigDecimal(206);
            assertThat(totalCredit).isEqualByComparingTo(expectedResult);
        }
    }
}
