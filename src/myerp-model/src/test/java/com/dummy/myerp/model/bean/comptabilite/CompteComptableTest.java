package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Classe de test pour le Bean {@link CompteComptable}
 */
@Tag("Compte_Comptable_Test")
@DisplayName("Tests unitaire pour le Bean CompteComptable")
public class CompteComptableTest {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Instance d'une liste de compte
    private List<CompteComptable> vListCompteComptable;



    /*
     * =================================================================================================================
     *                                              Methodes d'initialisation
     * =================================================================================================================
     */

    @BeforeEach
    public void initList() {
        vListCompteComptable = new ArrayList<>();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes de test
     * =================================================================================================================
     */

    @Test
    @DisplayName(
            "Si on fournit une liste de CompteComptable, " +
            "lorsqu'on veut en recuperer un par son numero, " +
            "alors le bon compte est retourne.")
    public void givenListOfCompteComptable_whenGetByNumero_thenReturnTheCorrectCompteComptable() {
        // GIVEN
        vListCompteComptable.add(new CompteComptable(1, "Un libelle"));
        vListCompteComptable.add(new CompteComptable(2, "Un deuxième libelle"));
        vListCompteComptable.add(new CompteComptable(3, "Un troisième libelle"));
        vListCompteComptable.add(new CompteComptable(4, "Un quatrième libelle"));

        // WHEN
        CompteComptable actualResult = CompteComptable.getByNumero(vListCompteComptable, 3);

        // THEN
        assertThat(actualResult.getLibelle()).isEqualTo("Un troisième libelle");
    }


    @Test
    @DisplayName(
            "Si on fournit un numero ne correspondant a aucun compte de la liste" +
            "lorsqu'on veut en recuperer un par son numero, " +
            "alors on recupere un objet null.")
    public void givenANumberNotInTheListOfCompteComptable_whenGetByNumero_thenReturnNull() {
        // GIVEN
        vListCompteComptable.add(new CompteComptable(1, "Un libelle"));
        vListCompteComptable.add(new CompteComptable(2, "Un deuxième libelle"));
        vListCompteComptable.add(new CompteComptable(3, "Un troisième libelle"));
        vListCompteComptable.add(new CompteComptable(4, "Un quatrième libelle"));

        // WHEN
        CompteComptable actualResult = CompteComptable.getByNumero(vListCompteComptable, -1);

        // THEN
        assertThat(actualResult).isNull();
    }


    @Test
    @DisplayName(
            "Si on fournit une liste vide, " +
            "lorsqu'on veut en recuperer un par son numero, " +
            "alors on recupere un objet null.")
    public void givenAnEmptyListOfCompteComptable_whenGetByNumero_thenReturnNull() {
        // GIVEN -- Nothing to do

        // WHEN
        CompteComptable actualResult = CompteComptable.getByNumero(vListCompteComptable, 1);

        // THEN
        assertThat(actualResult).isNull();
    }
}
