package com.dummy.myerp.model.bean.comptabilite;

import com.dummy.myerp.technical.util.validation.annotations.MontantComptable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;


/**
 * Bean representant une Ligne d'ecriture comptable.
 */
public class LigneEcritureComptable {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Le compte associe a la ligne d'ecriture comptable
    @NotNull
    private CompteComptable compteComptable;

    // Le libelle de la ligne d'ecriture comptable
    @Size(max = 200)
    private String libelle;

    // Le montant au debit de la ligne d'ecriture comptable
    @MontantComptable
    private BigDecimal debit;

    // Le montant au credit de la ligne d'ecriture comptable
    @MontantComptable
    private BigDecimal credit;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie une nouvelle {@link LigneEcritureComptable ligne d'ecriture comptable}.
     */
    public LigneEcritureComptable() {
    }


    /**
     * Instancie une nouvelle {@link LigneEcritureComptable ligne d'ecriture comptable} avec une valeur dans chacun des
     * attributs.
     *
     * @param pCompteComptable - Le {@link CompteComptable compte} associe
     * @param pLibelle - Le libelle de la {@link LigneEcritureComptable ligne d'ecriture}
     * @param pDebit - Le montant au debit de la {@link LigneEcritureComptable ligne d'ecriture}
     * @param pCredit - Le montant au credit de la {@link LigneEcritureComptable ligne d'ecriture}
     */
    public LigneEcritureComptable(
            CompteComptable pCompteComptable, String pLibelle, BigDecimal pDebit, BigDecimal pCredit) {
        compteComptable = pCompteComptable;
        libelle = pLibelle;
        debit = pDebit;
        credit = pCredit;
    }


    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    // ================================================ CompteComptable ================================================

    /**
     * Getter de l'attribut compteComptable
     *
     * @return {@link CompteComptable compteComptable}
     */
    public CompteComptable getCompteComptable() {
        return compteComptable;
    }

    /**
     * Setter de l'attribut libelle
     *
     * @param pCompteComptable - Le nouveau {@link CompteComptable compte} de la
     * {@link LigneEcritureComptable ligne d'ecriture comptable}.
     */
    public void setCompteComptable(CompteComptable pCompteComptable) {
        compteComptable = pCompteComptable;
    }


    // ==================================================== Libelle ====================================================

    /**
     * Getter de l'attribut libelle
     *
     * @return {@link String libelle}
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Setter de l'attribut libelle
     *
     * @param pLibelle - Le nouveau {@link String libelle} de la {@link LigneEcritureComptable ligne d'ecriture comptable}.
     */
    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }


    // ===================================================== Debit =====================================================

    /**
     * Getter de l'attribut debit
     *
     * @return {@link BigDecimal debit}
     */
    public BigDecimal getDebit() {
        return debit;
    }

    /**
     * Setter de l'attribut debit
     *
     * @param pDebit - Le nouveau montant au {@link BigDecimal debit} de la
     * {@link LigneEcritureComptable ligne d'ecriture comptable}.
     */
    public void setDebit(BigDecimal pDebit) {
        debit = pDebit;
    }


    // ===================================================== Credit ====================================================

    /**
     * Getter de l'attribut credit
     *
     * @return {@link BigDecimal credit}
     */
    public BigDecimal getCredit() {
        return credit;
    }

    /**
     * Setter de l'attribut credit
     *
     * @param pCredit - Le nouveau montant au {@link BigDecimal credit} de la
     * {@link LigneEcritureComptable ligne d'ecriture comptable}.
     */
    public void setCredit(BigDecimal pCredit) {
        credit = pCredit;
    }


    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
            .append("compteComptable=").append(compteComptable)
            .append(vSEP).append("libelle='").append(libelle).append('\'')
            .append(vSEP).append("debit=").append(debit)
            .append(vSEP).append("credit=").append(credit)
            .append("}");
        return vStB.toString();
    }
}
