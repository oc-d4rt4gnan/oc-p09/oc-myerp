package com.dummy.myerp.model.bean.comptabilite;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Bean representant une sequence pour les references {@link EcritureComptable d'ecriture comptable}
 */
public class SequenceEcritureComptable {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Le journal comptable associe
    @NotNull
    private JournalComptable journalComptable;

    // L'annee du journal comptable (max pourrait etre l'annee courante)
    @NotNull @Min(1000) @Max(9999)
    private Integer annee;

    // La derniere valeur du journal au niveau version
    private Integer derniereValeur;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie une nouvelle {@link SequenceEcritureComptable sequence d'ecriture comptable}.
     */
    public SequenceEcritureComptable() {
    }


    /**
     * Instancie une nouvelle {@link SequenceEcritureComptable sequence d'ecriture comptable}.
     *
     * @param pJournalComptable - Le {@link JournalComptable journal} associe.
     * @param pAnnee - L'annee du {@link JournalComptable journal}.
     * @param pDerniereValeur - La derniere version du {@link JournalComptable journal}.
     */
    public SequenceEcritureComptable(JournalComptable pJournalComptable, Integer pAnnee, Integer pDerniereValeur) {
        journalComptable = pJournalComptable;
        annee = pAnnee;
        derniereValeur = pDerniereValeur;
    }



    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    // =============================================== JournalComptable ================================================

    /**
     * Getter de l'attribut journalComptable
     *
     * @return {@link JournalComptable journalComptable}
     */
    public JournalComptable getJournalComptable() {
        return journalComptable;
    }

    /**
     * Setter de l'attribut journalComptable
     *
     * @param pJournalComptable - Le nouveau {@link JournalComptable journal} associe a la
     * {@link SequenceEcritureComptable sequence d'ecriture comptable}.
     */
    public void setJournalComptable(JournalComptable pJournalComptable) {
        this.journalComptable = pJournalComptable;
    }


    // ==================================================== Annee ======================================================

    /**
     * Getter de l'attribut annee
     *
     * @return {@link Integer annee}
     */
    public Integer getAnnee() {
        return annee;
    }

    /**
     * Setter de l'attribut annee
     *
     * @param pAnnee - La nouvelle {@link Integer annee} du {@link JournalComptable journal d'ecriture comptable}.
     */
    public void setAnnee(Integer pAnnee) {
        annee = pAnnee;
    }


    // ================================================ DerniereValeur =================================================

    /**
     * Getter de l'attribut derniereValeur
     *
     * @return {@link Integer derniereValeur}
     */
    public Integer getDerniereValeur() {
        return derniereValeur;
    }

    /**
     * Setter de l'attribut derniereValeur
     *
     * @param pDerniereValeur - Le nouvelle {@link Integer version} du {@link JournalComptable journal d'ecriture comptable}.
     */
    public void setDerniereValeur(Integer pDerniereValeur) {
        derniereValeur = pDerniereValeur;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
            .append("codeJournalComptable").append(journalComptable.getCode())
            .append(vSEP).append("annee=").append(annee)
            .append(vSEP).append("derniereValeur=").append(derniereValeur)
            .append("}");
        return vStB.toString();
    }
}
