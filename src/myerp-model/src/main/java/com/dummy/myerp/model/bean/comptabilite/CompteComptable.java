package com.dummy.myerp.model.bean.comptabilite;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;


/**
 * Bean representant un Compte Comptable
 */
public class CompteComptable {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private static Logger logger = LogManager.getLogger(CompteComptable.class);

    // Le numero du compte
    @NotNull
    private Integer numero;

    // Le libelle du compte
    @NotNull @Size(min = 1, max = 150)
    private String libelle;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie un nouveau {@link CompteComptable compte comptable}.
     */
    public CompteComptable() {
    }


    /**
     * Instancie un nouveau {@link CompteComptable compte comptable} avec son numero.
     *
     * @param pNumero - Le numero du {@link CompteComptable compte}.
     */
    public CompteComptable(Integer pNumero) {
        numero = pNumero;
    }


    /**
     * Instancie un nouveau {@link CompteComptable compte comptable} avec son numero et un libelle.
     *
     * @param pNumero - Le numero du {@link CompteComptable compte}.
     * @param pLibelle - Le libelle du {@link CompteComptable compte}.
     */
    public CompteComptable(Integer pNumero, String pLibelle) {
        numero = pNumero;
        libelle = pLibelle;
    }



    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    // ==================================================== Numero =====================================================

    /**
     * Getter de l'attribut numero
     *
     * @return {@link Integer numero}
     */
    public Integer getNumero() {
        return numero;
    }

    /**
     * Setter de l'attribut numero
     *
     * @param pNumero - Le nouveau {@link Integer numero} du {@link CompteComptable compte}
     */
    public void setNumero(Integer pNumero) {
        numero = pNumero;
    }


    // ==================================================== Libelle ====================================================

    /**
     * Getter de l'attribut libelle
     *
     * @return {@link String libelle}
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Setter de l'attribut libelle
     *
     * @param pLibelle - Le nouveau {@link String libelle} du {@link CompteComptable compte}
     */
    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
            .append("numero=").append(numero)
            .append(vSEP).append("libelle='").append(libelle).append('\'')
            .append("}");
        return vStB.toString();
    }


    /**
     * Renvoie le {@link CompteComptable compte} de numero {@code pNumero} s'il est present dans la liste
     *
     * @param pList - La liste ou chercher le {@link CompteComptable compte}
     * @param pNumero - Le numero du {@link CompteComptable compte} a chercher
     *
     * @return Le {@link CompteComptable compte} ou {@code null}
     */
    public static CompteComptable getByNumero(List<? extends CompteComptable> pList, Integer pNumero) {
        logger.trace("Recherche d'un compte comptable dans la liste : {}", pList);
        CompteComptable vRetour = null;
        for (CompteComptable vBean : pList) {
            if (vBean != null && Objects.equals(vBean.getNumero(), pNumero)) {
                vRetour = vBean;
                break;
            }
        }

        if (vRetour != null) {
            logger.debug("Le numéro {} correspond au compte {}", pNumero, vRetour);
        } else {
            logger.debug("Le numéro {} correspond au compte null", pNumero);
        }

        return vRetour;
    }
}
