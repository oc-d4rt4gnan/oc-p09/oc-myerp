package com.dummy.myerp.model.bean.comptabilite;


import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Bean representant une {@link EcritureComptable ecriture comptable}
 */
public class EcritureComptable {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private Logger logger = LogManager.getLogger(EcritureComptable.class);

    // L'id de l'ecriture comptable}.
    private Integer id;

    // Le journal de l'ecriture comptable
    @NotNull
    private JournalComptable journal;

    // La reference de l'ecriture comptable
    @Pattern(regexp = "[A-Z]{2}-\\d{4}/\\d{5}")
    private String reference;

    // La date de l'ecriture comptable
    @NotNull
    private Date date;

    // Le libelle de l'ecriture comptable
    @NotNull @Size(min = 1, max = 200)
    private String libelle;

    // La liste des lignes d'ecriture comptable.
    @Valid @Size(min = 2)
    private List<LigneEcritureComptable> listLigneEcriture = new ArrayList<>();



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie une nouvelle {@link EcritureComptable ecriture comptable}.
     */
    public EcritureComptable() {
        // Constructeur vide
    }



    /*
     * =================================================================================================================
     *                                                 Getters | Setters
     * =================================================================================================================
     */

    // ======================================================= ID ======================================================

    /**
     * Getter de l'attribut id
     *
     * @return {@link Integer id}
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter de l'attribut id
     *
     * @param pId - Le nouvel {@link Integer id} de {@link EcritureComptable l'ecriture comptable}
     */
    public void setId(Integer pId) {
        id = pId;
    }


    // ==================================================== Journal ====================================================

    /**
     * Getter de l'attribut journal
     *
     * @return {@link JournalComptable journal}
     */
    public JournalComptable getJournal() {
        return journal;
    }

    /**
     * Setter de l'attribut journal
     *
     * @param pJournal - Le nouveau {@link JournalComptable journal} de {@link EcritureComptable l'ecriture comptable}
     */
    public void setJournal(JournalComptable pJournal) {
        journal = pJournal;
    }


    // =================================================== Reference ===================================================

    /**
     * Getter de l'attribut reference
     *
     * @return {@link String reference}
     */
    public String getReference() {
        return reference;
    }

    /**
     * Setter de l'attribut reference
     *
     * @param pReference - La nouvelle {@link String reference} de {@link EcritureComptable l'ecriture comptable}
     */
    public void setReference(String pReference) {
        reference = pReference;
    }


    // ====================================================== Date =====================================================

    /**
     * Getter de l'attribut date
     *
     * @return {@link Date date}
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter de l'attribut date
     *
     * @param pDate - La nouvelle {@link Date date} de {@link EcritureComptable l'ecriture comptable}
     */
    public void setDate(Date pDate) {
        date = pDate;
    }


    // ===================================================== Libelle ===================================================

    /**
     * Getter de l'attribut libelle
     *
     * @return {@link String libelle}
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Setter de l'attribut libelle
     *
     * @param pLibelle - Le nouveau {@link String libelle} de {@link EcritureComptable l'ecriture comptable}
     */
    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }


    // =============================================== ListLigneEcriture ===============================================

    /**
     * Getter de l'attribut listLigneEcriture
     *
     * @return {@link List}{@literal <}{@link LigneEcritureComptable}{@literal >}
     */
    public List<LigneEcritureComptable> getListLigneEcriture() {
        return listLigneEcriture;
    }

    /**
     * Setter de l'attribut listLigneEctiture
     *
     * @param pListLigneEcriture - La nouvelle {@link List liste} de {@link LigneEcritureComptable ligne d'ecriture} de
     * {@link EcritureComptable l'ecriture comptable}
     */
    public void setListLigneEcriture(List<LigneEcritureComptable> pListLigneEcriture) {
        listLigneEcriture = pListLigneEcriture;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    /**
     * Calcul et renvoie le total des montants au debit des lignes d'ecriture
     *
     * @return {@link BigDecimal}, {@link BigDecimal#ZERO} si aucun montant au debit
     */
    public BigDecimal getTotalDebit() {
        logger.trace("Calcul du total des lignes au débit de l'écriture comptable {}", id);
        BigDecimal vRetour = listLigneEcriture
                .stream()
                .filter(e -> e.getDebit() != null)
                .map(LigneEcritureComptable::getDebit)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        logger.debug("Total des lignes au débit : {}", vRetour);
        return vRetour;
    }


    /**
     * Calcul et renvoie le total des montants au credit des lignes d'ecriture
     *
     * @return {@link BigDecimal}, {@link BigDecimal#ZERO} si aucun montant au credit
     */
    public BigDecimal getTotalCredit() {
        logger.trace("Calcul du total des lignes au crédit de l'écriture comptable {}", id);
        BigDecimal vRetour = listLigneEcriture
                .stream()
                .filter(e -> e.getCredit() != null)
                .map(LigneEcritureComptable::getCredit)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        logger.debug("Total des lignes au crédit : {}", vRetour);
        return vRetour;
    }


    /**
     * Renvoie si l'ecriture est equilibree (TotalDebit = TotalCredit)
     *
     * @param totalDebit - Valeur totale des montants au debit
     * @param totalCredit - Valeur totale des montants au credit
     *
     * @return boolean
     */
    public boolean isEquilibree(BigDecimal totalDebit, BigDecimal totalCredit) {
        totalDebit = totalDebit == null ? new BigDecimal(0) : totalDebit;
        totalCredit = totalCredit == null ? new BigDecimal(0) : totalCredit;

        int vEquilibre = totalDebit.compareTo(totalCredit);
        logger.debug("L'écriture comptable est-elle équilibrée ? {}", vEquilibre);

        return vEquilibre == 0 ;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
            .append("id=").append(id)
            .append(vSEP).append("journal=").append(journal)
            .append(vSEP).append("reference='").append(reference).append('\'')
            .append(vSEP).append("date=").append(date)
            .append(vSEP).append("libelle='").append(libelle).append('\'')
            .append(vSEP).append("totalDebit=").append(this.getTotalDebit().toPlainString())
            .append(vSEP).append("totalCredit=").append(this.getTotalCredit().toPlainString())
            .append(vSEP).append("listLigneEcriture=[\n")
            .append(StringUtils.join(listLigneEcriture, "\n")).append("\n]")
            .append("}");
        return vStB.toString();
    }
}
