package com.dummy.myerp.model.bean.comptabilite;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;


/**
 * Bean representant un Journal Comptable
 */
public class JournalComptable {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private static Logger logger = LogManager.getLogger(JournalComptable.class);

    // Le code du journal
    @NotNull @Size(min = 1, max = 5)
    private String code;

    // Le libelle du journal
    @NotNull @Size(min = 1, max = 150)
    private String libelle;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie un nouveau {@link JournalComptable journal comptable}.
     */
    public JournalComptable() {
    }


    /**
     * Instancie un nouveau {@link JournalComptable journal comptable} avec un code et un libelle.
     *
     * @param pCode - Le code du {@link JournalComptable journal}.
     * @param pLibelle - Le libelle du {@link JournalComptable journal}.
     */
    public JournalComptable(String pCode, String pLibelle) {
        code = pCode;
        libelle = pLibelle;
    }



    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    // ===================================================== Code ======================================================

    /**
     * Getter de l'attribut code
     *
     * @return {@link String code}
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter de l'attribut code
     *
     * @param pCode - Le nouveau {@link String code} du {@link JournalComptable journal}.
     */
    public void setCode(String pCode) {
        code = pCode;
    }


    // =================================================== Libelle =====================================================

    /**
     * Getter de l'attribut libelle
     *
     * @return {@link String libelle}
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Setter de l'attribut libelle
     *
     * @param pLibelle - Le nouveau {@link String libelle} du {@link JournalComptable journal}.
     */
    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
            .append("code='").append(code).append('\'')
            .append(vSEP).append("libelle='").append(libelle).append('\'')
            .append("}");
        return vStB.toString();
    }


    /**
     * Renvoie le {@link JournalComptable} de code {@code pCode} s'il est present dans la liste
     *
     * @param pList - La liste ou chercher le {@link JournalComptable}.
     * @param pCode - Le code du {@link JournalComptable} a chercher.
     *
     * @return Le {@link JournalComptable journal} ou {@code null}
     */
    public static JournalComptable getByCode(List<? extends JournalComptable> pList, String pCode) {
        logger.trace("Recherche d'un journal comptable dans la liste : {}", pList);
        JournalComptable vRetour = null;
        for (JournalComptable vBean : pList) {
            if (vBean != null && Objects.equals(vBean.getCode(), pCode)) {
                vRetour = vBean;
                break;
            }
        }

        if (vRetour != null) {
            logger.debug("Le code {} correspond au journal {}", pCode, vRetour);
        } else {
            logger.debug("Le code {} correspond au journal null", pCode);
        }

        return vRetour;
    }
}
