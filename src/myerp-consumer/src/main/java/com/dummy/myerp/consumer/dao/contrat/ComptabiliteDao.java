package com.dummy.myerp.consumer.dao.contrat;

import com.dummy.myerp.consumer.dao.impl.db.dao.ComptabiliteDaoImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;

import java.util.List;
import java.util.Optional;


/**
 * Interface de DAO des objets du package Comptabilite
 */
public interface ComptabiliteDao {

    /*
     * =================================================================================================================
     *                                                   INSTANCE
     * =================================================================================================================
     */

    /**
     * Renvoie une instance unique de la {@link ComptabiliteDao DAO de comptabilite}.
     * (Singleton Pattern)
     *
     * @return {@link ComptabiliteDao}
     */
    static ComptabiliteDao getInstance() {
        return ComptabiliteDaoImpl.getInstance();
    }



    /*
     * =================================================================================================================
     *                                                     SELECT
     * =================================================================================================================
     */

    /**
     * Renvoie la liste des {@link CompteComptable comptes comptables}
     *
     * @return {@link List}{@literal <}{@link CompteComptable}{@literal >}
     */
    List<CompteComptable> getListCompteComptable();


    /**
     * Renvoie la liste des {@link JournalComptable journaux comptables}
     *
     * @return {@link List}{@literal <}{@link JournalComptable}{@literal >}
     */
    List<JournalComptable> getListJournalComptable();


    /**
     * Renvoie la liste des {@link EcritureComptable ecritures comptables}
     *
     * @return {@link List}{@literal <}{@link EcritureComptable}{@literal >}
     */
    List<EcritureComptable> getListEcritureComptable();


    /**
     * Renvoie {@link EcritureComptable l'ecriture comptable} de reference {@code pReference}.
     *
     * @param pReference - La reference de {@link EcritureComptable l'ecriture comptable}
     *
     * @return {@link Optional}{@literal <}{@link EcritureComptable}{@literal >}
     */
    Optional<EcritureComptable> getEcritureComptableByRef(String pReference);


    /**
     * Charge la liste des {@link com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable lignes d'une ecriture comptable}
     * {@code pEcritureComptable}
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} contenant les
     * {@link com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable lignes d'ecriture}.
     */
    void loadListLigneEcriture(EcritureComptable pEcritureComptable);


    /**
     * Retourne la derniere {@link SequenceEcritureComptable sequence d'ecriture comptable} associee au code du
     * {@link JournalComptable journal comptable} et a l'annee de {@link EcritureComptable l'ecriture comptable}.
     *
     * @param pJournalComptableCode - Le code du {@link JournalComptable journal}.
     * @param pYear - L'annee de {@link EcritureComptable l'ecriture comptable}.
     *
     * @return {@link Optional}{@literal <}{@link SequenceEcritureComptable}{@literal >}
     */
    Optional<SequenceEcritureComptable> getSequenceEcritureComptableByJournalComptableCodeAndEcritureYear(
            String pJournalComptableCode, int pYear);



    /*
     * =================================================================================================================
     *                                                    INSERT
     * =================================================================================================================
     */

    /**
     * Sauvegarde une nouvelle {@link EcritureComptable ecriture comptable} en base de donnees.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a sauvegarder.
     */
    void insertEcritureComptable(EcritureComptable pEcritureComptable);


    /**
     * Sauvegarde une nouvelle {@link SequenceEcritureComptable sequence d'ecriture comptable} en base de donnees.
     *
     * @param pSequenceEcritureComptable - La {@link SequenceEcritureComptable sequence d'ecriture comptable} a sauvegarder.
     */
    void insertSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable);



    /*
     * =================================================================================================================
     *                                                    UPDATE
     * =================================================================================================================
     */

    /**
     * Met a jour {@link EcritureComptable l'ecriture comptable} en base de donnees.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} modifiee a enregistrer.
     */
    void updateEcritureComptable(EcritureComptable pEcritureComptable);


    /**
     * Mat a jour une {@link SequenceEcritureComptable sequence d'ecriture comptable} en base de donnees.
     *
     * @param pSequenceEcritureComptable - La {@link SequenceEcritureComptable sequence d'ecriture comptable} a enregistrer.
     */
    void updateSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable);



    /*
     * =================================================================================================================
     *                                                    DELETE
     * =================================================================================================================
     */

    /**
     * Supprime {@link EcritureComptable l'ecriture comptable} d'id {@code pId}.
     *
     * @param pId - l'id de {@link EcritureComptable l'ecriture comptable}.
     */
    void deleteEcritureComptable(Integer pId);


    /**
     * Supprime tous les elements de la base de donnees
     */
    void cleanDatabase();

}
