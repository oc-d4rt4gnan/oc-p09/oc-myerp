package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;

import java.util.List;


/**
 * Cache DAO de {@link JournalComptable}
 */
public class JournalComptableDaoCache {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // La liste des journaux comptable.
    private List<JournalComptable> listJournalComptable;



    /*
     * =================================================================================================================
     *                                                   Constructeurs
     * =================================================================================================================
     */
    /**
     * Instancie un nouveau {@link JournalComptableDaoCache cache dao de journal comptable}.
     */
    public JournalComptableDaoCache() {}



    /*
     * =================================================================================================================
     *                                                      Methodes
     * =================================================================================================================
     */

    /**
     * Permet de recuperer un {@link JournalComptable journal comptable} par son code {@code pCode}.
     *
     * @param pCode - Le code du {@link JournalComptable}.
     *
     * @return {@link JournalComptable} ou {@code null}
     */
    public JournalComptable getByCode(String pCode) {
        if (listJournalComptable == null) {
            listJournalComptable = ConsumerHelper
                    .getDaoProxy()
                    .getComptabiliteDao()
                    .getListJournalComptable();
        }

        return JournalComptable.getByCode(listJournalComptable, pCode);
    }
}
