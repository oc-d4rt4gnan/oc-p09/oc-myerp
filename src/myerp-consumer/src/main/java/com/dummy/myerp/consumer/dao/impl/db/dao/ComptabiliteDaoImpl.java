package com.dummy.myerp.consumer.dao.impl.db.dao;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite.*;
import com.dummy.myerp.consumer.db.AbstractDbConsumer;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.config.SqlConfig;
import com.dummy.myerp.technical.util.exception.TechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Types;
import java.util.List;
import java.util.Optional;


/**
 * Implementation de l'interface {@link ComptabiliteDao}
 */
public class ComptabiliteDaoImpl extends AbstractDbConsumer implements ComptabiliteDao {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private Logger logger = LogManager.getLogger(ComptabiliteDaoImpl.class);

    // Classe de configuration permettant de recuperer les requetes SQL
    private SqlConfig vSqlConfig;

    // Une instance de JdbcTemplate
    private JdbcTemplate vJdbcTemplate;

    // Une instance de NamedParameterJdbcTemplate
    private NamedParameterJdbcTemplate vNamedParameterJdbcTemplate;

    // Une instance du RowMapper de CompteComptable
    private CompteComptableRM vCompteComptableRM;

    // Une instance du RowMapper de JournalComptable
    private JournalComptableRM vJournalComptableRM;

    // Une instance du RowMapper de EcritureComptable
    private EcritureComptableRM vEcritureComptableRM;

    // Une instance du RowMapper de LigneEcritureComptable
    private LigneEcritureComptableRM vLigneEcritureComptableRM;

    // Une instance du RowMapper de SequenceEcritureComptable
    private SequenceEcritureComptableRM vSequenceEcritureComptableRM;

    // Instance unique de la classe (design pattern Singleton)
    private static final ComptabiliteDaoImpl INSTANCE = new ComptabiliteDaoImpl();



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @return {@link ComptabiliteDaoImpl}
     */
    public static ComptabiliteDaoImpl getInstance() {
        return ComptabiliteDaoImpl.INSTANCE;
    }


    /**
     * Creer et configure une instance de la classe.
     */
    private ComptabiliteDaoImpl() {
        super();

        try {
            vSqlConfig = new SqlConfig();
        } catch (TechnicalException e) {
            logger.error(e.getMessage());
        }

        vJdbcTemplate = new JdbcTemplate(this.getDataSource());
        vNamedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());
        vCompteComptableRM = new CompteComptableRM();
        vJournalComptableRM = new JournalComptableRM();
        vEcritureComptableRM = new EcritureComptableRM();
        vLigneEcritureComptableRM = new LigneEcritureComptableRM();
        vSequenceEcritureComptableRM = new SequenceEcritureComptableRM();
    }



    /*
     * =================================================================================================================
     *                                                Methodes
     * =================================================================================================================
     */

    // ==================================================== SELECT =====================================================
    /**
     * {@inheritDoc}
     */
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return vJdbcTemplate.query(vSqlConfig.getSQLSelectListCompteComptable(), vCompteComptableRM);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<JournalComptable> getListJournalComptable() {
        return vJdbcTemplate.query(vSqlConfig.getSQLSelectListJournalComptable(), vJournalComptableRM);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return vJdbcTemplate.query(vSqlConfig.getSQLSelectListEcritureComptable(), vEcritureComptableRM);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<EcritureComptable> getEcritureComptableByRef(String pReference) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("reference", pReference);

        try {
            return Optional.ofNullable(
                vNamedParameterJdbcTemplate.queryForObject(
                        vSqlConfig.getSQLSelectEcritureComptableByRef(), vSqlParams, vEcritureComptableRM
                )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void loadListLigneEcriture(EcritureComptable pEcritureComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("ecriture_id", pEcritureComptable.getId());

        List<LigneEcritureComptable> vList = vNamedParameterJdbcTemplate
                .query(vSqlConfig.getSQLLoadListLigneEcriture(), vSqlParams, vLigneEcritureComptableRM);

        pEcritureComptable.getListLigneEcriture().clear();
        pEcritureComptable.getListLigneEcriture().addAll(vList);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<SequenceEcritureComptable> getSequenceEcritureComptableByJournalComptableCodeAndEcritureYear(
            String pJournalComptableCode, int pYear) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("journal_code", pJournalComptableCode);
        vSqlParams.addValue("ecriture_year", pYear);

        Optional<SequenceEcritureComptable> vOptionalReturn;
        try {
            SequenceEcritureComptable vSequenceEcritureComptable = vNamedParameterJdbcTemplate.queryForObject(
                    vSqlConfig.getSQLSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year(),
                    vSqlParams,
                    vSequenceEcritureComptableRM
            );
            vOptionalReturn = Optional.of(vSequenceEcritureComptable);
        } catch (EmptyResultDataAccessException e) {
            vOptionalReturn = Optional.empty();
        }

        return vOptionalReturn;
    }



    // ==================================================== INSERT =====================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) {
        // ===== Insertion Journal Comptable
        try {
            this.insertJournalComptable(pEcritureComptable.getJournal());
        } catch (DuplicateKeyException duplicatException) {
            this.updateJournalComptable(pEcritureComptable.getJournal());
        }

        // ===== Ecriture Comptable
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("journal_code", pEcritureComptable.getJournal().getCode());
        vSqlParams.addValue("reference", pEcritureComptable.getReference());
        vSqlParams.addValue("date", pEcritureComptable.getDate(), Types.DATE);
        vSqlParams.addValue("libelle", pEcritureComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLInsertEcritureComptable(), vSqlParams);

        // ===== Recuperation de l'id
        Integer vId = this.queryGetSequenceValuePostgreSQL("myerp.ecriture_comptable_id_seq", Integer.class);
        pEcritureComptable.setId(vId);

        // ===== Liste des lignes d'ecriture
        this.insertListLigneEcritureComptable(pEcritureComptable);
    }


    /**
     * Sauvegarde les {@link LigneEcritureComptable lignes d'ecriture} de {@link EcritureComptable l'ecriture comptable}
     * en base de donnees.
     *
     * @param pEcritureComptable - {@link EcritureComptable l'ecriture comptable} contenant les
     * {@link LigneEcritureComptable lignes d'ecriture}
     */
    private void insertListLigneEcritureComptable(EcritureComptable pEcritureComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("ecriture_id", pEcritureComptable.getId());

        int vLigneId = 0;
        for (LigneEcritureComptable vLigne : pEcritureComptable.getListLigneEcriture()) {
            // ===== Insertion des comptes comptables
            try {
                this.insertCompteComptable(vLigne.getCompteComptable());
            } catch (DuplicateKeyException duplicateException) {
                this.updateCompteComptable(vLigne.getCompteComptable());
            }

            // ===== Insertion des lignes d'ecriture
            vLigneId++;
            vSqlParams.addValue("ligne_id", vLigneId);
            vSqlParams.addValue("compte_comptable_numero", vLigne.getCompteComptable().getNumero());
            vSqlParams.addValue("libelle", vLigne.getLibelle());
            vSqlParams.addValue("debit", vLigne.getDebit());

            vSqlParams.addValue("credit", vLigne.getCredit());

            vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLInsertListLigneEcritureComptable(), vSqlParams);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void insertSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("journal_code", pSequenceEcritureComptable.getJournalComptable().getCode());
        vSqlParams.addValue("annee", pSequenceEcritureComptable.getAnnee());
        vSqlParams.addValue("derniere_valeur", pSequenceEcritureComptable.getDerniereValeur());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLInsertSequenceEcritureComptable(), vSqlParams);
    }


    /**
     * Sauvegarde un {@link JournalComptable journal comptable} en base de donnees
     *
     * @param pJournalComptable - Le {@link JournalComptable journal} a sauvegarder
     */
    private void insertJournalComptable(JournalComptable pJournalComptable) throws DuplicateKeyException {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("code", pJournalComptable.getCode());
        vSqlParams.addValue("libelle", pJournalComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLInsertJournalComptable(), vSqlParams);
    }


    /**
     * Sauvegarde un {@link CompteComptable compte comptable} en base de donnees
     *
     * @param pCompteComptable - Le {@link CompteComptable compte} a sauvegarder
     */
    private void insertCompteComptable(CompteComptable pCompteComptable) throws DuplicateKeyException {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("numero", pCompteComptable.getNumero());
        vSqlParams.addValue("libelle", pCompteComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLInsertCompteComptable(), vSqlParams);
    }



    // ==================================================== UPDATE =====================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) {
        // ===== Ecriture Comptable
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("id", pEcritureComptable.getId());
        vSqlParams.addValue("journal_code", pEcritureComptable.getJournal().getCode());
        vSqlParams.addValue("reference", pEcritureComptable.getReference());
        vSqlParams.addValue("date", pEcritureComptable.getDate(), Types.DATE);
        vSqlParams.addValue("libelle", pEcritureComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLUpdateEcritureComptable(), vSqlParams);

        // ===== Liste des lignes d'ecriture
        this.deleteListLigneEcritureComptable(pEcritureComptable.getId());
        this.insertListLigneEcritureComptable(pEcritureComptable);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("journal_code", pSequenceEcritureComptable.getJournalComptable().getCode());
        vSqlParams.addValue("annee", pSequenceEcritureComptable.getAnnee());
        vSqlParams.addValue("derniere_valeur", pSequenceEcritureComptable.getDerniereValeur());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLUpdateSequenceEcritureComptable(), vSqlParams);
    }


    /**
     * Met a jour un {@link JournalComptable journal comptable} en base de donnees
     *
     * @param pJournalComptable - Le {@link JournalComptable journal} a mettre a jour
     */
    private void updateJournalComptable(JournalComptable pJournalComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("code", pJournalComptable.getCode());
        vSqlParams.addValue("libelle", pJournalComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLUpdateJournalComptable(), vSqlParams);
    }


    /**
     * Met a jour un {@link CompteComptable compte comptable} en base de donnees
     *
     * @param pCompteComptable - Le {@link CompteComptable compte} a mettre a jour
     */
    private void updateCompteComptable(CompteComptable pCompteComptable) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("numero", pCompteComptable.getNumero());
        vSqlParams.addValue("libelle", pCompteComptable.getLibelle());

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLUpdateCompteComptable(), vSqlParams);
    }



    // ==================================================== DELETE =====================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        // ===== Suppression des lignes d'ecriture
        this.deleteListLigneEcritureComptable(pId);

        // ===== Suppression de l'ecriture
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("id", pId);

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLDeleteEcritureComptable(), vSqlParams);
    }


    /**
     * Supprime les {@link LigneEcritureComptable lignes d'ecriture} de {@link EcritureComptable l'ecriture comptable}
     * d'id {@code pEcritureId}
     *
     * @param pEcritureId - id de l'ecriture comptable
     */
    protected void deleteListLigneEcritureComptable(Integer pEcritureId) {
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("ecriture_id", pEcritureId);

        vNamedParameterJdbcTemplate.update(vSqlConfig.getSQLDeleteListLigneEcritureComptable(), vSqlParams);
    }


    /**
     * {@inheritDoc}
     */
    public void cleanDatabase() {
        vJdbcTemplate.update(vSqlConfig.getSQLDeleteAllLigneEcritureComptable());
        vJdbcTemplate.update(vSqlConfig.getSQLDeleteAllSequenceEcritureComptable());
        vJdbcTemplate.update(vSqlConfig.getSQLDeleteAllCompteComptable());
        vJdbcTemplate.update(vSqlConfig.getSQLDeleteAllEcritureComptable());
        vJdbcTemplate.update(vSqlConfig.getSQLDeleteAllJournalComptable());
    }
}
