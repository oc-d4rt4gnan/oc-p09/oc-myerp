package com.dummy.myerp.consumer.db;

import com.dummy.myerp.technical.config.DataSourceConfig;
import com.dummy.myerp.technical.util.exception.TechnicalException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;


/**
 * Classe mere des classes de Consumer DB
 */
public abstract class AbstractDbConsumer {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // La classe de configuration de la DataSource
    private DataSourceConfig vDataSourceConfig;



    /*
     * =================================================================================================================
     *                                                   Constructeurs
     * =================================================================================================================
     */

    /**
     * Creer une instance de la classe.
     */
    protected AbstractDbConsumer() {
        super();
        try {
            this.vDataSourceConfig = new DataSourceConfig();
        } catch (TechnicalException e) {
            e.printStackTrace();
        }
    }



    /*
     * =================================================================================================================
     *                                                     Methodes
     * =================================================================================================================
     */

    /**
     * Renvoie la {@link DataSource} associe demandee
     *
     * @return {@link DataSource}
     */
    protected DataSource getDataSource() {
        return vDataSourceConfig.getDataSource();
    }


    /**
     * Renvoie le derniere valeur utilise d'une sequence
     *
     * <p><i><b>Attention : </b>Methode specifique au SGBD PostgreSQL</i></p>
     *
     * @param <T> : La classe de la valeur de la sequence.
     * @param pSeqName : Le nom de la sequence dont on veut recuperer la valeur
     * @param pSeqValueClass : Classe de la valeur de la sequence
     *
     * @return la derniere valeur de la sequence
     */
    protected <T> T queryGetSequenceValuePostgreSQL(String pSeqName, Class<T> pSeqValueClass) {

        JdbcTemplate vJdbcTemplate = new JdbcTemplate(getDataSource());
        String vSeqSQL = "SELECT last_value FROM " + pSeqName;
        T vSeqValue = vJdbcTemplate.queryForObject(vSeqSQL, pSeqValueClass);

        return vSeqValue;
    }
}
