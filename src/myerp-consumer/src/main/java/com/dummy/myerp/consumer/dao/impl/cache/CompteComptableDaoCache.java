package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;

import java.util.List;


/**
 * Cache DAO de {@link CompteComptable}
 */
public class CompteComptableDaoCache {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // La liste des comptes comptables.
    private List<CompteComptable> listCompteComptable;



    /*
     * =================================================================================================================
     *                                                   Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie un nouveau {@link CompteComptableDaoCache cache dao de compte comptable}.
     */
    public CompteComptableDaoCache() {}



    /*
     * =================================================================================================================
     *                                                      Methodes
     * =================================================================================================================
     */

    /**
     * Permet de recuperer un {@link CompteComptable compte comptable} par son numero.
     *
     * @param pNumero - Le numero du {@link CompteComptable compte comptable}.
     *
     * @return {@link CompteComptable} ou {@code null}
     */
    public CompteComptable getByNumero(Integer pNumero) {
        if (listCompteComptable == null) {
            listCompteComptable = ConsumerHelper.getDaoProxy().getComptabiliteDao().getListCompteComptable();
        }

        return CompteComptable.getByNumero(listCompteComptable, pNumero);
    }
}