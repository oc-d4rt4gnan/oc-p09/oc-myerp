package com.dummy.myerp.consumer;

import com.dummy.myerp.consumer.dao.contrat.DaoProxy;


/**
 * Classe d'aide pour les classes du module consumer
 */
public abstract class ConsumerHelper {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // La DaoProxy a utiliser pour acceder aux autres classes de DAO
    private static DaoProxy daoProxy = DaoProxy.getInstance();



    /*
     * =================================================================================================================
     *                                                 Getters | Setters
     * =================================================================================================================
     */

    /**
     * Renvoie l'instance de {@link DaoProxy} a l'interieur du module.
     *
     * @return {@link DaoProxy}
     */
    public static DaoProxy getDaoProxy() {
        return daoProxy;
    }
}