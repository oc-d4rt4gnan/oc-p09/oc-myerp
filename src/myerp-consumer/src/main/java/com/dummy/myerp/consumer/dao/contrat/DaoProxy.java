package com.dummy.myerp.consumer.dao.contrat;


import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;

/**
 * Interface du Proxy d'acces a la couche DAO
 */
public interface DaoProxy {

    /*
     * =================================================================================================================
     *                                                   INSTANCE
     * =================================================================================================================
     */

    /**
     * Renvoie une instance unique du {@link DaoProxy proxy} d'acces a la couche Consumer-DAO.
     * (Singleton Pattern)
     *
     * @return {@link DaoProxy}
     */
    static DaoProxy getInstance() {
        return DaoProxyImpl.getInstance();
    }



    /*
     * =================================================================================================================
     *                                                GETTERS | SETTERS
     * =================================================================================================================
     */

    /**
     * Renvoie une instance de {@link ComptabiliteDao}
     *
     * @return {@link ComptabiliteDao}
     */
    ComptabiliteDao getComptabiliteDao();

}
