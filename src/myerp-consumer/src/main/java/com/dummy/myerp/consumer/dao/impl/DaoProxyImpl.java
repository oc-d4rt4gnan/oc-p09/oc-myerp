package com.dummy.myerp.consumer.dao.impl;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;


/**
 * Implementation du Proxy d'acces a la couche DAO.
 */
public final class DaoProxyImpl implements DaoProxy {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Interface de la dao de comptabilite
    private static ComptabiliteDao comptabiliteDao;

    // Instance unique de la classe (design pattern Singleton)
    private static final DaoProxyImpl INSTANCE = new DaoProxyImpl();



    /*
     * =================================================================================================================
     *                                                   Constructeurs
     * =================================================================================================================
     */

    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @return {@link DaoProxyImpl}
     */
    public static DaoProxyImpl getInstance() {
        comptabiliteDao = ComptabiliteDao.getInstance();
        return DaoProxyImpl.INSTANCE;
    }

    /**
     * Creer une instance de la classe.
     */
    private DaoProxyImpl() {
        super();
    }



    /*
     * =================================================================================================================
     *                                                 Getters | Setters
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    public ComptabiliteDao getComptabiliteDao() {
        return comptabiliteDao;
    }
}