package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.util.exception.FunctionalException;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

/**
 * Classe de test d'integration pour la classe
 * {@link com.dummy.myerp.business.contrat.manager.ComptabiliteManager ComptabiliteManager}
 */
@Tag("Comptabilite_Manager_IT")
@DisplayName("Test l'integration des differents composant depuis la couche Business")
class ComptabiliteManagerImplIT {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Classe manager a tester
    private ComptabiliteManagerImpl manager;

    // Instance du proxy d'acces a la couche Consumer-DAO
    private DaoProxy daoProxy = DaoProxy.getInstance();

    // Ecriture comptable globale pour la classe de test
    private EcritureComptable vEcritureComptable;

    // Reference globale pour la classe de test
    private String vReference = "BQ-2016/00003";



    /*
     * =================================================================================================================
     *                                              Methodes d'initialisation
     * =================================================================================================================
     */

    @BeforeEach
    void prepare() {
        // initialisation du manager
        manager = new ComptabiliteManagerImpl();

        // Creation d'un premier compte
        CompteComptable vCompteDebit = new CompteComptable();
        vCompteDebit.setLibelle("compte debit");
        vCompteDebit.setNumero(1);

        // Creation d'un deuxieme compte
        CompteComptable vCompteCredit = new CompteComptable();
        vCompteCredit.setLibelle("compte credit");
        vCompteCredit.setNumero(2);

        // Creation d'une premiere ligne d'ecriture comptable
        LigneEcritureComptable vLigneEcritureDebit = new LigneEcritureComptable();
        vLigneEcritureDebit.setLibelle("debit");
        vLigneEcritureDebit.setDebit(new BigDecimal(100));
        vLigneEcritureDebit.setCredit(new BigDecimal(0));
        vLigneEcritureDebit.setCompteComptable(vCompteDebit);

        // Creation d'une deuxieme ligne d'ecriture comptable
        LigneEcritureComptable vLigneEcritureCredit = new LigneEcritureComptable();
        vLigneEcritureCredit.setLibelle("credit");
        vLigneEcritureCredit.setDebit(new BigDecimal(0));
        vLigneEcritureCredit.setCredit(new BigDecimal(100));
        vLigneEcritureCredit.setCompteComptable(vCompteCredit);

        // Creation d'une liste de ligne d'ecriture comptable
        List<LigneEcritureComptable> vListLigneEcriture = new ArrayList<>();
        vListLigneEcriture.add(vLigneEcritureDebit);
        vListLigneEcriture.add(vLigneEcritureCredit);

        // Creation d'un journal comptable
        JournalComptable vJournalComptable = new JournalComptable();
        vJournalComptable.setCode("BQ");
        vJournalComptable.setLibelle("Journal de Banque");

        // Creation d'une date
        Calendar vCalendar = new GregorianCalendar();
        vCalendar.set(2016, Calendar.MARCH,24);
        Date vDate = vCalendar.getTime();

        // Creation d'une ecriture comptable
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setReference(vReference);
        vEcritureComptable.setDate(vDate);
        vEcritureComptable.setJournal(vJournalComptable);
        vEcritureComptable.setLibelle("libelle");
        vEcritureComptable.setListLigneEcriture(vListLigneEcriture);

        // Purge de la base de donnees
        this.cleanDatabase();
    }


    @AfterEach
    void remove() {
        this.cleanDatabase();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes de test
     * =================================================================================================================
     */

    @Test
    @DisplayName(
            "Si on fournit une ecriture comptable valide, " +
            "lorsqu'on verifie le context, " +
            "alors aucune exception ne doit etre levee.")
    void givenAValidEcritureComptable_whenCheckEcritureComptableContext_thenNoThrownAnyException() {
        // PREPARE
        String vReference = "BQ-2016/00003";
        Optional<EcritureComptable> vEcritureComptableDB = daoProxy.getComptabiliteDao()
                .getEcritureComptableByRef(vReference);
        vEcritureComptableDB.ifPresent(ecritureComptable ->
                daoProxy.getComptabiliteDao().deleteEcritureComptable(ecritureComptable.getId()));

        // GIVEN
        EcritureComptable vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setReference(vReference);
        vEcritureComptable.setDate(new Date());
        vEcritureComptable.setJournal(new JournalComptable());
        vEcritureComptable.setLibelle("");

        // THEN
        assertThatCode(
                // WHEN
                () -> manager.checkEcritureComptableContext(vEcritureComptable)
        ).doesNotThrowAnyException();
    }


    @Test
    @Tag("CRUD_TEST")
    @DisplayName(
            "Si on fournit une ecriture comptable, " +
            "lorsqu'on souhaite l'enregistrer en base de donnees, " +
            "alors l'ecriture comptable doit bien etre enregistree en base de donnees.")
    void givenAnEcritureComptable_whenInsertEcritureComptable_thenTheEcritureComptableWasInsertedInDB()
            throws FunctionalException {
        // GIVEN

        // WHEN
        manager.insertEcritureComptable(vEcritureComptable);

        // THEN
        Optional<EcritureComptable> vOptionalEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(vReference);
        EcritureComptable vEcritureComptableInDB = null;
        if (vOptionalEcritureComptable.isPresent()) {
            vEcritureComptableInDB = vOptionalEcritureComptable.get();
        }
        assertThat(vEcritureComptable.getReference()).isEqualTo(vEcritureComptableInDB.getReference());
    }


    @Test
    @Tag("CRUD_TEST")
    @DisplayName(
            "Si on fournit une ecriture comptable modifiee, " +
            "lorsqu'on veut enregistrer la modification en base de donnees, " +
            "alors la modification doit bien etre enregistree en base de donnees.")
    void givenAnEcritureComptable_whenUpdateEcritureComptable_thenTheEcritureComptableWasUpdatedInDB()
            throws FunctionalException {
        // PREPARE
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // GIVEN
        String vNewLabel = "nouveau libelle";
        vEcritureComptable.setLibelle(vNewLabel);

        // WHEN
        manager.updateEcritureComptable(vEcritureComptable);

        // THEN
        Optional<EcritureComptable> vOptionalEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(vReference);
        String vLabelInDB = "";
        if (vOptionalEcritureComptable.isPresent()) {
            vLabelInDB = vOptionalEcritureComptable.get().getLibelle();
        }
        assertThat(vEcritureComptable.getLibelle()).isEqualTo(vLabelInDB);
    }


    @Test
    @Tag("CRUD_TEST")
    @DisplayName(
            "Si on fournit un id, " +
            "lorsqu'on veut supprimer une ecriture comptable, " +
            "alors l'ecriture doit bien etre supprimee en base de donnees.")
    void givenAnId_whenDeleteEcritureComptable_thenTheEcritureComptableWasDeletedInDB() {
        // PREPARE
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);
        Optional<EcritureComptable> vOptionalEcritureComptable = daoProxy.getComptabiliteDao()
                .getEcritureComptableByRef(vReference);

        // GIVEN
        Integer vEcritureComptableId = null;
        if (vOptionalEcritureComptable.isPresent()) {
            vEcritureComptableId = vOptionalEcritureComptable.get().getId();
        }

        // WHEN
        manager.deleteEcritureComptable(vEcritureComptableId);

        // THEN
        vOptionalEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(vReference);
        assertThat(vOptionalEcritureComptable.isPresent()).isEqualTo(false);
    }


    @Test
    @Tag("GETTER_TEST")
    @DisplayName(
            "Si on veux recuperer la liste des comptes comptables, " +
            "alors tous les comptes comptables doivent être remontes.")
    void givenNothing_whenGetListCompteComptable_thenItReturnsTheListOfAllCompteComptable() {
        // PREPARE
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // GIVEN -- Nothing to do

        // WHEN
        List<CompteComptable> vListeCompteComptable = manager.getListCompteComptable();

        // THEN
        assertThat(vListeCompteComptable.get(0)).isInstanceOf(CompteComptable.class);
        assertThat(vListeCompteComptable.size()).isEqualTo(2);
    }


    @Test
    @Tag("GETTER_TEST")
    @DisplayName(
            "Si on veux recuperer la liste des journaux comptables, " +
            "alors tous les journaux comptables doivent être remontes.")
    void givenNothing_whenGetListJournalComptable_thenItReturnsTheListOfAllJournalComptable() {
        // PREPARE
        // Code BQ
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // Code CQ
        vEcritureComptable.getJournal().setCode("CQ");
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // Code DQ
        vEcritureComptable.getJournal().setCode("DQ");
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // GIVEN -- Nothing to do

        // WHEN
        List<JournalComptable> vListeJournalComptable = manager.getListJournalComptable();

        // THEN
        assertThat(vListeJournalComptable.get(0)).isInstanceOf(JournalComptable.class);
        assertThat(vListeJournalComptable.size()).isEqualTo(3);
    }


    @Test
    @Tag("GETTER_TEST")
    @DisplayName(
            "Si on veux recuperer la liste des ecritures comptables, " +
            "alors toutes les ecritures comptables doivent être remontees.")
    void givenNothing_whenGetListEcritureComptable_thenItReturnsTheListOfAllEcritureComptable() {
        // PREPARE
        for (int i = 0; i < 9; i++) {
            daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);
        }

        // GIVEN -- Nothing to do

        // WHEN
        List<EcritureComptable> vListeEcritureComptable = manager.getListEcritureComptable();

        // THEN
        assertThat(vListeEcritureComptable.get(0)).isInstanceOf(EcritureComptable.class);
        assertThat(vListeEcritureComptable.size()).isEqualTo(9);
    }


    @Test
    @DisplayName(
            "Si on fournit une ecriture comptable, " +
            "lorsqu'on ajoute une reference, " +
            "alors une sequence d'ecriture comptable est enregistree en base de donnees.")
    void givenAnEcritureComptable_whenAddReference_thenTheNewReferenceIsSaved() throws FunctionalException {
        // GIVEN
        vEcritureComptable.getJournal().setCode("MX");
        daoProxy.getComptabiliteDao().insertEcritureComptable(vEcritureComptable);

        // WHEN
        manager.addReference(vEcritureComptable);

        // THEN
        assertThat(daoProxy.getComptabiliteDao()
                .getSequenceEcritureComptableByJournalComptableCodeAndEcritureYear("MX", 2016)
        ).isPresent();
        assertThat(vEcritureComptable.getReference()).isEqualTo("MX-2016/00001");
    }



    /*
     * =================================================================================================================
     *                                                 Methodes privees
     * =================================================================================================================
     */

    // Methode de purge de la base de donnees de test
    private void cleanDatabase() {
        daoProxy.getComptabiliteDao().cleanDatabase();
    }

}
