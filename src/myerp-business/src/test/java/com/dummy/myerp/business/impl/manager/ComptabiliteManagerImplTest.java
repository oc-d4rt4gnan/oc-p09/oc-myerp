package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.util.exception.FunctionalException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

/**
 * Classe de test pour la classe {@link com.dummy.myerp.business.contrat.manager.ComptabiliteManager},
 * utilisant {@link Mockito}
 */
@Tag("Comptabilite_Manager_TU")
@DisplayName("Tests pour la classe ComptabiliteManager.")
@ExtendWith(MockitoExtension.class)
class ComptabiliteManagerImplTest {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Instance de l'implementation de ComptabiliteManager
    @InjectMocks
    private ComptabiliteManagerImpl manager;

    // Instance du proxy d'acces a la couche Consumer-DAO
    @Spy
    private DaoProxy daoProxy;

    // Mock du ComptabiliteDao
    @Mock
    private ComptabiliteDao comptabiliteDao;



    /*
     * =================================================================================================================
     *                                              Methodes d'initialisation
     * =================================================================================================================
     */

    @BeforeEach
    void prepare() {
        MockitoAnnotations.initMocks(this);
        manager = new ComptabiliteManagerImpl();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes de test
     * =================================================================================================================
     */

    // =============================================== GetYearOfDateFor ================================================

    @Nested
    @Tag("Get_Date_Test")
    @DisplayName("Doit retourner l'annee de l'objet Date ou lever une exception si l'objet est null.")
    class GetDateTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec une date valide, " +
                "lorsqu'on tente de recuperer l'annee, " +
                "alors la bonne annee est retourne.")
        void givenEcritureComptableWithValidDate_whenGetYearOfDateFor_thenReturnTheYearOfTheDate()
                throws FunctionalException {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            Date vDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
            vEcritureComptable.setDate(vDate);

            // WHEN
            int actualResult = manager.getYearOfDateFor(vEcritureComptable);

            // THEN
            assertThat(actualResult).isEqualTo(2014);
        }


        @Test
        @DisplayName(
                "Si on fournit un objet null, " +
                "lorsqu'on tente de recuperer l'annee, " +
                "alors une exception fonctionelle avec le code ERG_4-1 est levee.")
        void givenNullObject_whenGetYearOfDateFor_thenThrowAFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = null;

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.getYearOfDateFor(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-1");
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec une date null, " +
                "lorsqu'on tente de recuperer l'annee, " +
                "alors une exception fonctionelle avec le code ERG_4-2 est levee.")
        void givenEcritureComptableWithAnEmptyDate_whenGetYearOfDateFor_thenThrowAFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.getYearOfDateFor(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-2");
        }
    }


    // ================================================== GetNewValue ==================================================

    @Nested
    @Tag("Get_New_Value_Test")
    @DisplayName(
            "Doit retourner la derniere valeur + 1 de la sequence du journal comptable pour l'annee en question, " +
            "ou retourner 1 dans le cas ou la derniere valeur serait null.")
    class GetNewValueTest {

        @Test
        @DisplayName(
                "Si on fournit une sequence d'ecriture comptable avec pour derniere valeur 40, " +
                "lorsqu'on veut recuperer la nouvelle valeur, " +
                "alors on doit obtenir 41.")
        void givenSequenceEcritureComptableWithALastValueOf40_whenGetNewValue_thenReturn41()
                throws FunctionalException {
            // GIVEN
            SequenceEcritureComptable vSequenceEcritureComptable = new SequenceEcritureComptable();
            vSequenceEcritureComptable.setDerniereValeur(40);

            // WHEN
            int actualResult = manager.getNewValue(vSequenceEcritureComptable);

            // THEN
            assertThat(actualResult).isEqualTo(41);
        }


        @Test
        @DisplayName(
                "Si on fournit une sequence d'ecriture comptable null, " +
                "lorsqu'on veut recuperer la nouvelle valeur, " +
                "alors on doit obtenir 1.")
        void givenANullSequenceEcritureComptable_whenGetNewValue_thenReturn1() throws FunctionalException {
            // GIVEN
            SequenceEcritureComptable vSequenceEcritureComptable = null;

            // WHEN
            int actualResult = manager.getNewValue(vSequenceEcritureComptable);

            // THEN
            assertThat(actualResult).isEqualTo(1);
        }


        @ParameterizedTest(name = "La derniere valeur {0} doit lever une exception fonctionnelle.")
        @ValueSource(ints = {-16, 0})
        @DisplayName(
                "Si on fournit une sequence d'ecriture comptable avec une valeur null ou negative, " +
                "lorsqu'on veut recuperer la nouvelle valeur, " +
                "alors une exception fonctionnelle avec le code ERG_4-3 doit etre levee.")
        void givenSequenceEcritureComptableWithANegativeLastValue_whenGetNewValue_thenReturnTheCorrectFunctionalException(int args) {
            // GIVEN
            SequenceEcritureComptable vSequenceEcritureComptable = new SequenceEcritureComptable();
            vSequenceEcritureComptable.setDerniereValeur(args);
            JournalComptable vJournalComptable = new JournalComptable();
            vJournalComptable.setCode("MonCode");
            vSequenceEcritureComptable.setJournalComptable(vJournalComptable);

            // WHEN
            Throwable thrown = catchThrowable(() -> {
                manager.getNewValue(vSequenceEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-3");
        }
    }


    // ============================================== CreateNewReference ===============================================

    @Nested
    @Tag("Create_New_Reference_Test")
    @DisplayName("Doit retourner la nouvelle reference au format XX-AAAA/#####.")
    class CreateNewReferenceTest {

        @Test
        @DisplayName(
                "Si on fournit un journal de banque (BQ) avec une premiere entree en 2016, " +
                "lorsqu'on veut obtenir la reference associee, " +
                "alors on doit obtenir BQ-2016/00001.")
        void givenAJournalCodeAndAYearOfEcritureComptableAndAValue_whenCreateNewReference_thenReturnTheCorrectReference()
                throws FunctionalException {
            // GIVEN
            String vJournalCode = "BQ";
            int vYearEcritureComptable = 2016;
            int vNewValue = 1;

            // WHEN
            String actualResult = manager.createNewReference(vJournalCode, vYearEcritureComptable, vNewValue);

            // THEN
            assertThat(actualResult).isEqualTo("BQ-2016/00001");
        }


        @Test
        @DisplayName(
                "Si on fournit un code de journal null, " +
                "lorsqu'on veut obtenir la reference de l'ecriture comptable, " +
                "alors une exception fonctionnelle avec le code ERG_4-4 doit etre levee.")
        void givenANullValueOfJournalCode_whenCreateNewReference_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            String vJournalCode = null;
            int vYearEcritureComptable = 2016;
            int vNewValue = 32;

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.createNewReference(vJournalCode, vYearEcritureComptable, vNewValue);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-4");
        }


        @ParameterizedTest(name = "La chaine de caracteres \"{0}\" doit lever une exception fonctionnelle !")
        @ValueSource(strings = {"ab", "12", "a1", "1a", "a", "1", "MYTESTS"})
        @DisplayName(
                "Si on fournit un code de journal avec de mauvais caracteres, " +
                "lorsqu'on veut obtenir la reference de l'ecriture comptable, " +
                "alors une exception fonctionnelle avec le code ERG_4-5 est levee.")
        void givenAWrongLogCode_whenCreateNewReference_thenThrowTheCorrectFunctionalException(String pJournalCode) {
            // GIVEN
            int vYearEcritureComptable = 2016;
            int vNewValue = 32;

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.createNewReference(pJournalCode, vYearEcritureComptable, vNewValue);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-5");
        }


        @ParameterizedTest(name = "L'annee {0} doit lever une exception fonctionnelle !")
        @ValueSource(ints = {-2016, 310, 10500})
        @DisplayName(
                "Si on fournit une annee au mauvais format, " +
                "lorsqu'on veut obtenir la reference de l'ecriture comptable, " +
                "alors une exception fonctionnelle avec le code ERG_4-6 est levee.")
        void givenYearWithWrongFormat_whenCreateNewReference_thenThrowTheCorrectFunctionalException(int pYearEcritureComptable) {
            // GIVEN
            String vJournalCode = "BQ";
            int vNewValue = 24;

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.createNewReference(vJournalCode, pYearEcritureComptable, vNewValue);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-6");
        }


        @Test
        @DisplayName(
                "Si on fournit une valeur negative pour le numero de la sequence, " +
                "lorsqu'on veut obtenir la reference de l'ecriture comptable, " +
                "alors une exception fonctionnelle avec le code ERG_4-7 est levee.")
        void givenNegativeValueForTheSequenceNumber_whenCreateNewReference_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            String vJournalCode = "BQ";
            int vYearEcritureComptable = 2016;
            int vNewValue = -11;

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.createNewReference(vJournalCode, vYearEcritureComptable, vNewValue);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_4-7");
        }
    }


    // ===================================== CheckValidationEcritureComptableUnit ======================================

    @Nested
    @Tag("Check_Validation_Ecriture_Comptable_Unit_Test")
    @DisplayName(
            "Doit lever une exception fonctionnelle si le validateur trouve une ou plusieurs regles de gestion non " +
            "respectees.")
    class CheckValidationEcritureComptableUnitTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable valide, " +
                "lorsqu'on la fait passer a la validation, " +
                "alors aucune exception n'est levee.")
        void givenAValidEcritureComptable_whenCheckValidationEcritureComptableUnit_thenNoExceptionsAreThrown() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new Date());
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1),null, new BigDecimal(123),null
                    )
            );
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), null, null, new BigDecimal(123)
                    )
            );

            // THEN
            assertThatCode(() -> {
                // WHEN
                manager.checkValidationEcritureComptableUnit(vEcritureComptable);
            }).doesNotThrowAnyException();
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable non valide, " +
                "lorsqu'on la fait passer a la validation, " +
                "alors une exception fonctionnelle avec le code ERG-0 est levee.")
        void givenANonValidEcritureComptable_whenCheckValidationEcritureComptableUnit_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkValidationEcritureComptableUnit(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG-0");
        }
    }


    // ============== RG_Compta_2 : Pour qu'une ecriture comptable soit valide, elle doit etre equilibree ==============

    @Nested
    @Tag("Check_Ecriture_Comptable_Unit_Is_Balanced_Test")
    @DisplayName("Doit lever une exception fonctionnelle si les lignes de l'ecriture comptable ne sont pas equilibrees.")
    class CheckEcritureComptableUnitIsBalancedTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable contenant des lignes equilibrees, " +
                "lorsqu'on verifie l'equilibre de l'ecriture comptable, " +
                "alors aucun exception n'est levee.")
        void givenEcritureComptableWithBalancedLines_whenCheckEcritureComptableUnitIsBalanced_thenNoExceptionsAreThrown() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), null, null, new BigDecimal(123)
                    )
            );

            // THEN
            assertThatCode(() -> {
                // WHEN
                manager.checkEcritureComptableUnitIsBalanced(vEcritureComptable);
            }).doesNotThrowAnyException();
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable contenant des lignes d'ecriture desequilibrees, " +
                "lorsqu'on verifie l'equilibre de l'ecriture comptable, " +
                "alors une exception fonctionnelle avec le code ERG_2-1 est levee.")
        void givenEcritureComptableWithUnbalancedLines_whenCheckEcritureComptableUnitIsBalanced_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new Date());
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), null, null, new BigDecimal(1234)
                    )
            );

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkEcritureComptableUnitIsBalanced(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_2-1");
        }
    }


    // ==== RG_Compta_3 : une ecriture comptable doit avoir au moins 2 lignes d'ecriture (1 au debit, 1 au credit) =====

    @Nested
    @Tag("Check_Validity_Of_List_Ligne_Ecriture_In_Ecriture_Comptable_Unit_Test")
    @DisplayName("Doit lever une exception fonctionnelle si la liste des lignes d'une ecriture comptable ne respecte " +
            "pas la regle de gestion RG3.")
    class CheckValidityOfListLigneEcritureInEcritureComptableUnitTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable contenant des lignes equilibrees, " +
                "lorsqu'on verifie la validite de la liste de lignes d'ecriture, " +
                "alors aucune exception n'est levee.")
        void givenEcritureComptableWithTwoCorrectLines_whenCheckValidityOfListLigneEcritureInEcritureComptableUnit_thenNoExceptionsAreThrown() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(2), null, null, new BigDecimal(123)
                    )
            );

            // THEN
            assertThatCode(() -> {
                // WHEN
                manager.checkEcritureComptableUnitIsBalanced(vEcritureComptable);
            }).doesNotThrowAnyException();
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable contant seulement une ligne, " +
                "lorsqu'on verifie la validite de la liste des lignes d'ecriture, " +
                "alors une exception fonctionnelle avec le code ERG_3-1 est levee.")
        void givenEcritureComptableWithOneLines_whenCheckValidityOfListLigneEcritureInEcritureComptableUnit_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new Date());
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkValidityOfListLigneEcritureInEcritureComptableUnit(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_3-1");
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec 2 lignes au debit, " +
                "lorsqu'on verifie la validite de la liste de lignes d'acriture, " +
                "alors une exception fonctionnelle avec le code ERG_3-1 est levee.")
        void givenEcritureComptableWith2DebitLines_whenCheckValidityOfListLigneEcritureInEcritureComptableUnit_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new Date());
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );
            vEcritureComptable.getListLigneEcriture().add(
                    new LigneEcritureComptable(
                            new CompteComptable(1), null, new BigDecimal(123), null
                    )
            );

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkValidityOfListLigneEcritureInEcritureComptableUnit(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_3-1");
        }
    }


    // ================================ RG_Compta_5 : format et contenu de la reference ================================

    @Nested
    @Tag("Check_Format_And_Content_Of_The_Ecriture_Comptable_Unit_Reference_Test")
    @DisplayName(
            "Doit lever une exception fonctionnelle si le format ou le contenu de la reference de " +
            "l'ecriture comptable ne correspond pas aux attributs de celle-ci.")
    class CheckFormatAndContentOfTheEcritureComptableUnitReferenceTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec la bonne reference, " +
                "lorsqu'on verifie le format et le contenu de cette reference, " +
                "alors aucune exception n'est levee.")
        void givenAnEcritureComptableWithRightReference_whenCheckFormatAndContentOfTheEcritureComptableUnitReference_thenNoExceptionsAreThrown() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            Date vDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
            vEcritureComptable.setDate(vDate);
            vEcritureComptable.setReference("AC-2014/00001");

            // THEN
            assertThatCode(() -> {
                // WHEN
                manager.checkFormatAndContentOfTheEcritureComptableUnitReference(vEcritureComptable);
            }).doesNotThrowAnyException();
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec une reference au mauvais format, " +
                "lorsqu'on verifie le format et le contenu de la reference, " +
                "alors une exception fonctionnelle avec le code ERG_5-1 est levee.")
        void givenAnEcritureComptableWithAReferenceToTheWrongFormat_whenCheckFormatAndContentOfTheEcritureComptableUnitReference_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            Date vDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
            vEcritureComptable.setDate(vDate);
            vEcritureComptable.setReference("AC201400001");

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkFormatAndContentOfTheEcritureComptableUnitReference(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_5-1");
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable avec la mauvaise reference, " +
                "lorsqu'on verifie le format et le contenu de la reference, " +
                "alors une exception fonctionnelle avec le code ERG_5.2 est levee.")
        void givenAnEcritureComptableWithWrongReference_whenCheckFormatAndContentOfTheEcritureComptableUnitReference_thenThrowTheCorrectFunctionalException() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            Date vDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
            vEcritureComptable.setDate(vDate);
            vEcritureComptable.setReference("BC-2015/00001");

            // WHEN
            Throwable thrown = Assertions.catchThrowable(() -> {
                manager.checkFormatAndContentOfTheEcritureComptableUnitReference(vEcritureComptable);
            });

            // THEN
            assertThat(thrown)
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_5-2");
        }
    }


    // ===================== RG_Compta_6 : La reference d'une ecriture comptable doit etre unique ======================

    @Nested
    @Tag("Check_Ecriture_Comptable_Context_Test")
    @DisplayName(
            "Doit lever une exception fonctionnelle si la reference de l'ecriture comptable est vide ou " +
            "existe deja en base de donnees.")
    class CheckEcritureComptableContextTest {

        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable vide, " +
                "lorsqu'on verifie le contexte, " +
                "alors une exception fonctionnelle avec le code ERG_6.2. est levee.")
        void givenAnEmptyEcritureComptable_whenCheckEcritureComptableContext_thenThrowERG6Exception() {
            // GIVEN
            EcritureComptable vEcritureComptable = new EcritureComptable();

            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                manager.checkEcritureComptableContext(vEcritureComptable);
            })
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_6-2");
        }


        @Test
        @DisplayName(
                "Si on fournit une reference existante en base de donnees, " +
                "lorsqu'on verifie le contexte, " +
                "alors une exception fonctionnelle avec le code ERG_6.1 est levee.")
        void givenAnExistingReference_whenCheckEcritureComptableContext_thenThrowERG6Exception() {
            // GIVEN
            // --- initialisation de l'ecriture comptable
            String vReference = "XX-AAAA/#####";
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setReference(vReference);

            // --- initialisation de l'objet retourne attendu avec le mock
            Optional<EcritureComptable> vReturnedEcritureComptable = Optional.of(vEcritureComptable);
            manager.setDaoProxy(daoProxy);

            Mockito.when(comptabiliteDao.getEcritureComptableByRef(vReference)).thenReturn(vReturnedEcritureComptable);
            Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                manager.checkEcritureComptableContext(vEcritureComptable);
            })
                    .isInstanceOf(FunctionalException.class)
                    .hasFieldOrPropertyWithValue("vErrorCode", "ERG_6-1");
        }


        @Test
        @DisplayName(
                "Si on fournit une ecriture comptable valide, " +
                "lorsqu'on verifie le contexte, " +
                "alors aucune exception n'est levee.")
        void givenAValidEcritureComptable_whenCheckEcritureComptableContext_thenNoExceptionAreThrown() {
            // GIVEN
            // --- initialisation de l'ecriture comptable
            String vReference = "XX-AAAA/#####";
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setReference(vReference);

            // --- initialisation de l'objet retourne attendu avec le mock
            Optional<EcritureComptable> vReturnedEcritureComptable = Optional.empty();
            manager.setDaoProxy(daoProxy);

            Mockito.when(comptabiliteDao.getEcritureComptableByRef(vReference)).thenReturn(vReturnedEcritureComptable);
            Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

            // THEN
            assertThatCode(() -> {
                // WHEN
                manager.checkEcritureComptableContext(vEcritureComptable);
            }).doesNotThrowAnyException();
        }
    }
}
