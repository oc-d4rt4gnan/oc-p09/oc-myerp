package com.dummy.myerp.business.contrat;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;


/**
 * Interface du Proxy d'acces a la couche Business
 */
public interface BusinessProxy {

    /**
     * Renvoie le manager du package Comptabilite.
     *
     * @return {@link ComptabiliteManager}
     */
    ComptabiliteManager getComptabiliteManager();

}
