package com.dummy.myerp.business.impl;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.manager.ComptabiliteManagerImpl;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;


/**
 * Implementation du Proxy d'acces a la couche Business.
 */
public class BusinessProxyImpl implements BusinessProxy {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Le Proxy d'acces a la couche Consumer-DAO
    private static DaoProxy daoProxy;

    // Instanciation du ComptabiliteManager.
    private ComptabiliteManager comptabiliteManager = new ComptabiliteManagerImpl();

    // Instance unique de la classe (design pattern Singleton)
    private static final BusinessProxyImpl INSTANCE = new BusinessProxyImpl();



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @return {@link BusinessProxyImpl}
     */
    public static BusinessProxyImpl getInstance() {
        if (daoProxy == null) {
            throw new UnsatisfiedLinkError("La classe BusinessProxyImpl n'a pas été initialisée.");
        }
        return BusinessProxyImpl.INSTANCE;
    }


    /**
     * Creer une instance de l'implementation du proxy de la couche Business.
     */
    protected BusinessProxyImpl() {
        super();
    }



    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public ComptabiliteManager getComptabiliteManager() {
        return comptabiliteManager;
    }
}