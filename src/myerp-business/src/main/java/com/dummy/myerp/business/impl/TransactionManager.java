package com.dummy.myerp.business.impl;

import com.dummy.myerp.technical.config.DataSourceConfig;
import com.dummy.myerp.technical.util.exception.TechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Classe de gestion des Transactions de persistance
 */
public class TransactionManager {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private static Logger logger = LogManager.getLogger(TransactionManager.class);

    // PlatformTransactionManager pour le DataSource MyERP
    private static PlatformTransactionManager ptmMyERP;

    // Instance unique de la classe (design pattern Singleton)
    private static final TransactionManager INSTANCE = new TransactionManager();



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @return {@link TransactionManager}
     */
    public static TransactionManager getInstance() {
        try {
            ptmMyERP = new DataSourceTransactionManager(new DataSourceConfig().getDataSource());
        } catch (TechnicalException e) {
            logger.error(e.getMessage());
        }
        return TransactionManager.INSTANCE;
    }


    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @param pPtmMyERP - Le {@link PlatformTransactionManager} a utiliser.
     *
     * @return L'instance unique de {@link TransactionManager}.
     */
    public static TransactionManager getInstance(PlatformTransactionManager pPtmMyERP) {
        ptmMyERP = pPtmMyERP;
        return TransactionManager.INSTANCE;
    }


    /**
     * Creer une instance du {@link TransactionManager}.
     */
    protected TransactionManager() {
        super();
    }



    /*
     * =================================================================================================================
     *                                                      Methodes
     * =================================================================================================================
     */

    /**
     * Demarre une transaction sur le DataSource MyERP
     *
     * @return TransactionStatus a passer aux methodes :
     *      <ul>
     *          <li>{@link #commitMyERP(TransactionStatus)}</li>
     *              <li>{@link #rollbackMyERP(TransactionStatus)}</li>
     *      </ul>
     */
    public TransactionStatus beginTransactionMyERP() {
        DefaultTransactionDefinition vTDef = new DefaultTransactionDefinition();
        vTDef.setName("Transaction_txManagerMyERP");
        vTDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        return ptmMyERP.getTransaction(vTDef);
    }

    /**
     * Commit la transaction sur le DataSource MyERP
     *
     * @param pTStatus - Retroune par la methode {@link #beginTransactionMyERP()}
     */
    public void commitMyERP(TransactionStatus pTStatus) {
        if (pTStatus != null) {
            ptmMyERP.commit(pTStatus);
        }
    }

    /**
     * Rollback la transaction sur le DataSource MyERP
     *
     * @param pTStatus - Retroune par la methode {@link #beginTransactionMyERP()}
     */
    public void rollbackMyERP(TransactionStatus pTStatus) {
        if (pTStatus != null) {
            ptmMyERP.rollback(pTStatus);
        }
    }
}
