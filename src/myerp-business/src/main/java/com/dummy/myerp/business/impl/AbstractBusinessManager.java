package com.dummy.myerp.business.impl;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;

import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


/**
 * Classe mere des Managers
 */
public abstract class AbstractBusinessManager {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Le Proxy d'acces a la couche Business
    private static BusinessProxy businessProxy = new BusinessProxyImpl();

    // Le Proxy d'acces a la couche Consumer-DAO
    private static DaoProxy daoProxy = DaoProxy.getInstance();

    // Le gestionnaire de Transaction
    private static TransactionManager transactionManager = TransactionManager.getInstance();



    /*
     * =================================================================================================================
     *                                                Getters | Setters
     * =================================================================================================================
     */

    // ================================================= BusinessProxy =================================================

    protected BusinessProxy getBusinessProxy() {
        return businessProxy;
    }


    // =================================================== DaoProxy ====================================================

    protected DaoProxy getDaoProxy() {
        return daoProxy;
    }

    /**
     * Set le Proxy d'acces a la couche Consumer-DAO
     *
     * @param pDaoProxy - une instance de {@link DaoProxy}
     */
    public void setDaoProxy(DaoProxy pDaoProxy) {
        daoProxy = pDaoProxy;
    }


    // ============================================== TransactionManager ===============================================

    protected TransactionManager getTransactionManager() {
        return transactionManager;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    protected Validator getConstraintValidator() {
        Configuration<?> vConfiguration = Validation.byDefaultProvider().configure();
        ValidatorFactory vFactory = vConfiguration.buildValidatorFactory();
        Validator vValidator = vFactory.getValidator();
        return vValidator;
    }
}