package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.util.exception.FunctionalException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.util.*;


/**
 * Comptabilite manager implementation.
 */
public class ComptabiliteManagerImpl extends AbstractBusinessManager implements ComptabiliteManager {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private Logger logger = LogManager.getLogger(ComptabiliteManagerImpl.class);



    /*
     * =================================================================================================================
     *                                                   Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie un nouveau {@link ComptabiliteManager manager de comptabilite}.
     */
    public ComptabiliteManagerImpl() {
    }



    /*
     * =================================================================================================================
     *                                                  Getters/Setters
     * =================================================================================================================
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return getDaoProxy().getComptabiliteDao().getListCompteComptable();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<JournalComptable> getListJournalComptable() {
        return getDaoProxy().getComptabiliteDao().getListJournalComptable();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return getDaoProxy().getComptabiliteDao().getListEcritureComptable();
    }



    /*
     * =================================================================================================================
     *                                                      Methodes
     * =================================================================================================================
     */

    // ================================================= addReference ==================================================

    /**
     * {@inheritDoc}
     */
    // Bien se referer a la JavaDoc de cette methode !
    /* Le principe :
            1.  Remonter depuis la persitance la derniere valeur de la sequence du journal pour l'annee de l'ecriture
                (table sequence_ecriture_comptable)

            2.  * S'il n'y a aucun enregistrement pour le journal pour l'annee concernee :
                    1. Utiliser le numero 1.
                * Sinon :
                    1. Utiliser la derniere valeur + 1
            3.  Mettre a jour la reference de l'ecriture avec la reference calculee (RG_Compta_5)
            4.  Enregistrer (insert/update) la valeur de la sequence en persitance
                (table sequence_ecriture_comptable)
     */
    // TODO à tester (Integration)
    @Override
    public synchronized void addReference(EcritureComptable pEcritureComptable) throws FunctionalException {
        // Init
        String vJournalCode = pEcritureComptable.getJournal().getCode();
        Optional<SequenceEcritureComptable> vOptionalSequenceEcritureComptable;
        int vYearEcritureComptable = this.getYearOfDateFor(pEcritureComptable);

        // ===== 1
        vOptionalSequenceEcritureComptable = getDaoProxy().getComptabiliteDao()
                .getSequenceEcritureComptableByJournalComptableCodeAndEcritureYear(vJournalCode, vYearEcritureComptable);

        SequenceEcritureComptable vSequenceEcritureComptable = vOptionalSequenceEcritureComptable.orElse(null);

        if (vSequenceEcritureComptable != null) {
            String vSequenceEcritureComptableToString = vSequenceEcritureComptable.toString();
            logger.debug("Récupération de la séquence d'écriture comptable : {}", vSequenceEcritureComptableToString);
        } else {
            logger.debug("Aucune séquence d'écriture comptable correspondante en base de données.");
        }

        int vNewValue = 0;
        vNewValue = this.getNewValue(vSequenceEcritureComptable);

        // ===== 3
        String vNewReference = this.createNewReference(vJournalCode, vYearEcritureComptable, vNewValue);
        pEcritureComptable.setReference(vNewReference);

        // ===== 4
        if (vSequenceEcritureComptable != null) {
            vSequenceEcritureComptable.setDerniereValeur(vNewValue);
            getDaoProxy().getComptabiliteDao().updateSequenceEcritureComptable(vSequenceEcritureComptable);
        } else {
            vSequenceEcritureComptable =
                    new SequenceEcritureComptable(pEcritureComptable.getJournal(), vYearEcritureComptable, vNewValue);
            getDaoProxy().getComptabiliteDao().insertSequenceEcritureComptable(vSequenceEcritureComptable);
        }

        logger.debug("Insertion en base de données de la séquence d'écriture comptable ...");
    }


    /**
     * Permet de recuperer l'annee contenu dans la {@link Date date} de {@link EcritureComptable l'ecriture comptable}.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} dont il faut recuperer l'annee.
     *
     * @return l'annee contenu dans une {@link Date date}.
     *
     * @throws FunctionalException Si la {@link Date date} ou {@link EcritureComptable l'ecriture comptable} est null.
     */
    protected int getYearOfDateFor(EcritureComptable pEcritureComptable) throws FunctionalException {
        if (pEcritureComptable != null) {
            if (pEcritureComptable.getDate() != null) {
                Calendar vCalendar = new GregorianCalendar();
                vCalendar.setTime(pEcritureComptable.getDate());
                int vYear = vCalendar.get(Calendar.YEAR);
                logger.debug("Année de l'écriture comptable : {}", vYear);
                return vYear;
            } else {
                throw new FunctionalException("ERG_4-2", "La date de l'écriture comptable est null !");
            }
        } else {
            throw new FunctionalException("ERG_4-1", "L'écriture comptable est null !");
        }
    }


    /**
     * Modifie la derniere valeur d'une {@link SequenceEcritureComptable sequence d'ecriture comptable}.
     *
     * @param pSequenceEcritureComptable - La {@link SequenceEcritureComptable sequence d'ecriture comptable} a modifier.
     *
     * @return la derniere valeur +1, s'il en existe une, sinon 1.
     *
     * @throws FunctionalException Si la derniere valeur trouvee est negative.
     */
    protected int getNewValue(SequenceEcritureComptable pSequenceEcritureComptable) throws FunctionalException {
        if (pSequenceEcritureComptable != null) {
            int vDerniereValeur = pSequenceEcritureComptable.getDerniereValeur();
            if (vDerniereValeur > 0) {
                int vNewValeur = vDerniereValeur + 1;
                logger.debug("Nouvelle valeur pour la séquence d'écriture comptable : {}", vNewValeur);
                return vNewValeur;
            } else {
                String vCode = pSequenceEcritureComptable.getJournalComptable().getCode();
                throw new FunctionalException("ERG_4-3", "La dernière valeur de la séquence d'écriture comptable du " +
                        "journal " + vCode + " est négative ou null !");
            }
        } else {
            logger.debug("Nouvelle valeur pour la séquence d'écriture comptable : 1");
            return 1;
        }
    }


    /**
     * Creer une nouvelle reference pour une {@link EcritureComptable ecriture comptable} a partir de son code de
     * {@link JournalComptable journal}, de l'annee {@link EcritureComptable d'ecriture} et d'une valeur de
     * {@link SequenceEcritureComptable sequence}.
     *
     * @param pJournalCode - Le code du {@link JournalComptable journal} associe a
     * {@link EcritureComptable l'ecriture comptable}.
     * @param pYearEcritureComptable - L'annee de {@link EcritureComptable l'ecriture comptable}.
     * @param pNewValue - La nouvelle derniere valeur de la {@link SequenceEcritureComptable sequence d'ecriture comptable}.
     *
     * @return la nouvelle reference d'une {@link EcritureComptable ecriture comptable}.
     *
     * @throws FunctionalException
     *  <ul>
     *      <li>Si le code du {@link JournalComptable journal} est null ou incorrect</li>
     *      <li>Si l'annee {@link EcritureComptable d'ecriture} n'est pas comprise entre 1000 et 9999</li>
     *      <li>Si la nouvelle valeur de {@link SequenceEcritureComptable sequence} est negative</li>
     *  </ul>
     */
    protected String createNewReference(String pJournalCode, int pYearEcritureComptable, int pNewValue)
            throws FunctionalException {
        if (pJournalCode == null) {
            throw new FunctionalException("ERG_4-4", "Le code du journal ne peut pas être null !");
        } else if (!pJournalCode.matches("[A-Z]{1,5}")) {
            throw new FunctionalException("ERG_4-5", "Le code du journal n'est pas au bon format !");
        } else if (pYearEcritureComptable < 1000 || pYearEcritureComptable > 9999) {
            throw new FunctionalException("ERG_4-6", "L'année doit être comprise entre 1000 et 9999 !");
        } else if (!(pNewValue > 0)) {
            throw new FunctionalException("ERG_4-7", "La valeur de la séquence doit être positive !");
        }

        String vNewReference = String.format("%s-%s/%05d", pJournalCode, pYearEcritureComptable, pNewValue);
        logger.debug("Nouvelle référence du journal {} de l'année {} : {}", pJournalCode,
                pYearEcritureComptable, vNewReference);

        return vNewReference;
    }



    // =========================================== Check Ecriture Comptable ============================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkValidationEcritureComptableUnit(pEcritureComptable);
        this.checkValidityOfListLigneEcritureInEcritureComptableUnit(pEcritureComptable);
        this.checkEcritureComptableUnitIsBalanced(pEcritureComptable);
        this.checkFormatAndContentOfTheEcritureComptableUnitReference(pEcritureComptable);
    }


    /**
     * Verifie que {@link EcritureComptable l'ecriture comptable} soit valide (Dont RG_Compta_7)
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a verifier.
     *
     * @throws FunctionalException Si {@link EcritureComptable l'ecriture comptable} ne respecte pas les regles de gestion
     */
    protected void checkValidationEcritureComptableUnit(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== Verification des contraintes unitaires sur les attributs de l'ecriture
        Set<ConstraintViolation<EcritureComptable>> vViolations = getConstraintValidator().validate(pEcritureComptable);
        if (!vViolations.isEmpty()) {
            throw new FunctionalException("ERG-0", "L'écriture comptable ne respecte pas les règles de gestion.",
                    new ConstraintViolationException(
                            "L'écriture comptable ne respecte pas les contraintes de validation",
                            vViolations));
        }
    }


    /**
     * Verifie l'equilibre des {@link LigneEcritureComptable lignes d'ecriture comptable} et qu'elles respectent la regle
     * de gestion fonctionnelle 2.
     *
     * <strong>RG_Compta_2 : </strong>
     * Pour qu'une {@link EcritureComptable ecriture comptable} soit valide, elle doit etre equilibree :
     * la somme des montants au credit des {@link LigneEcritureComptable lignes d'ecriture} doit etre egale a la somme
     * des montants au debit.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a verifier.
     *
     * @throws FunctionalException Si les {@link LigneEcritureComptable lignes d'ecriture comptable} ne sont pas equilibrees.
     * */
    protected void checkEcritureComptableUnitIsBalanced(EcritureComptable pEcritureComptable) throws FunctionalException {
        BigDecimal vTotalDebit = pEcritureComptable.getTotalDebit();
        BigDecimal vTotalCredit = pEcritureComptable.getTotalCredit();

        if (!pEcritureComptable.isEquilibree(vTotalDebit, vTotalCredit)) {
            throw new FunctionalException("ERG_2-1", "RG_Compta_2 : L'écriture comptable n'est pas équilibrée.");
        }
    }


    /**
     * Verifie le nombre de {@link LigneEcritureComptable ligne d'ecriture comptable} et leur contenu
     *
     * <strong>RG_Compta_3 : </strong>
     * Une {@link EcritureComptable ecriture comptable} doit contenir au moins deux
     * {@link LigneEcritureComptable lignes d'ecriture} : une au debit et une au credit.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a verifier.
     *
     * @throws FunctionalException S'il y a moins de 2 {@link LigneEcritureComptable lignes d'ecriture comptable} ou
     * s'il y a moins de 1 ligne au debit et 1 ligne au credit
     */
    protected void checkValidityOfListLigneEcritureInEcritureComptableUnit(EcritureComptable pEcritureComptable)
            throws FunctionalException {
        int vNbrCredit = 0;
        int vNbrDebit = 0;
        for (LigneEcritureComptable vLigneEcritureComptable : pEcritureComptable.getListLigneEcriture()) {
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getCredit(),
                    BigDecimal.ZERO)) != 0) {
                vNbrCredit++;
            }
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getDebit(),
                    BigDecimal.ZERO)) != 0) {
                vNbrDebit++;
            }
        }

        // On test le nombre de lignes car si l'ecriture a une seule ligne
        // avec un montant au debit et un montant au credit ce n'est pas valable
        logger.debug("Total de ligne au débit : {}", vNbrDebit);
        logger.debug("Total de ligne au crédit : {}", vNbrCredit);
        if (pEcritureComptable.getListLigneEcriture().size() < 2
                || vNbrCredit < 1
                || vNbrDebit < 1) {
            throw new FunctionalException("ERG_3-1", "RG_Compta_3 : L'écriture comptable doit avoir au moins deux lignes" +
                    " : une ligne au débit et une ligne au crédit.");
        }
    }


    /**
     * Verifie le format et le contenu de la reference d'une {@link EcritureComptable ecriture comptable}
     *
     * <strong>RG_Compta_5 : </strong>
     * La reference d'une {@link EcritureComptable ecriture comptable} est composee du code du {@link JournalComptable journal}
     * dans lequel figure {@link EcritureComptable l'ecriture} suivi de l'annee et d'un numero de
     * {@link SequenceEcritureComptable sequence} (propre a chaque {@link JournalComptable journal}) sur 5 chiffres
     * incremente automatiquement a chaque {@link EcritureComptable ecriture}.
     * Le formatage de la reference est : XX-AAAA/#####.
     * <br>
     * Ex : Journal de banque (BQ), ecriture au 31/12/2016
     * <pre>BQ-2016/00001</pre>
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a verifier.
     *
     * @throws FunctionalException Si la reference n'a pas le bon format ou que l'un des elements qui l'a compose n'est
     * pas correct
     */
    protected void checkFormatAndContentOfTheEcritureComptableUnitReference(EcritureComptable pEcritureComptable)
            throws FunctionalException {
        String vCode = pEcritureComptable.getJournal().getCode();
        int vYear = this.getYearOfDateFor(pEcritureComptable);

        String vReference = pEcritureComptable.getReference();
        logger.debug("Référence de l'écriture comptable : {}", vReference);

        if (!vReference.matches("[A-Z]{1,5}-\\d{4}/\\d{5}")) {
            throw new FunctionalException("ERG_5-1", "RG_Compta_5 : La référence n'est pas au bon format.");
        }

        String[] vReferenceChunk = vReference.split("[-/]");

        logger.debug("Tableau en sortie du découpage de la référence : {}", vReferenceChunk);

        if (!vReferenceChunk[0].equals(vCode) || !vReferenceChunk[1].equals(String.format("%s", vYear))) {
            throw new FunctionalException("ERG_5-2",
                    "RG_Compta_5 : La référence ne correspond pas à l'écriture qui lui est associée.");
        }
    }


    /**
     * Verifie l'unicite de {@link EcritureComptable l'ecriture comptable}.
     *
     * <strong>RG_Compta_6 : </strong>
     * La reference d'une {@link EcritureComptable ecriture comptable} doit etre unique,
     * il n'est pas possible de creer plusieurs {@link EcritureComptable ecritures} ayant la meme reference.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a verifier.
     *
     * @throws FunctionalException Si {@link EcritureComptable l'ecriture comptable} existe deja.
     */
    protected void checkEcritureComptableContext(EcritureComptable pEcritureComptable) throws FunctionalException {
        if (StringUtils.isNoneEmpty(pEcritureComptable.getReference())) {
            // Recherche d'une ecriture ayant la meme reference
            Optional<EcritureComptable> vECRef = getDaoProxy().getComptabiliteDao()
                    .getEcritureComptableByRef(pEcritureComptable.getReference());
            if (vECRef.isPresent()) {
                throw new FunctionalException("ERG_6-1",
                        "RG_Compta_6 : Une autre écriture comptable existe déjà avec la même référence.");
            }
        } else {
            throw new FunctionalException("ERG_6-2", "RG_Compta_6 : Aucune référence n'a été fourni.");
        }
    }


    // ============================================ CRUD EcritureComptable =============================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        this.checkEcritureComptableContext(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().insertEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().deleteEcritureComptable(pId);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }
}