package com.dummy.myerp.business.contrat.manager;

import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.util.exception.FunctionalException;

import java.util.List;


/**
 * Interface du manager du package comptabilite.
 */
public interface ComptabiliteManager {

    /**
     * Renvoie la liste des comptes comptables.
     *
     * @return {@link List}{@literal <}{@link CompteComptable}{@literal >}
     */
    List<CompteComptable> getListCompteComptable();


    /**
     * Renvoie la liste des journaux comptables.
     *
     * @return {@link List}{@literal <}{@link JournalComptable}{@literal >}
     */
    List<JournalComptable> getListJournalComptable();


    /**
     * Renvoie la liste des ecritures comptables.
     *
     * @return {@link List}{@literal <}{@link EcritureComptable}{@literal >}
     */
    List<EcritureComptable> getListEcritureComptable();


    /**
     * Ajoute une reference a {@link EcritureComptable l'ecriture comptable}.
     *
     * <strong>RG_Compta_5 : </strong>
     * La reference d'une {@link EcritureComptable ecriture comptable} est composee du code du
     * {@link JournalComptable journal} dans lequel figure {@link EcritureComptable l'ecriture} suivi de l'annee et d'un
     * numero de {@link SequenceEcritureComptable sequence} (propre a chaque {@link JournalComptable journal}) sur
     * 5 chiffres incremente automatiquement a chaque ecriture. Le formatage de la reference est : XX-AAAA/#####.
     * <br>
     * Ex : Journal de banque (BQ), acriture au 31/12/2016
     * <pre>BQ-2016/00001</pre>
     *
     * <p><strong>Attention :</strong> {@link EcritureComptable l'ecriture} n'est pas enregistree en persistance</p>
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} concernee
     */
    void addReference(EcritureComptable pEcritureComptable) throws FunctionalException;


    /**
     * Verifie que {@link EcritureComptable l'ecriture comptable} respecte les regles de gestion.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a tester
     *
     * @throws FunctionalException Si {@link EcritureComptable l'ecriture comptable} ne respecte pas les regles de gestion
     */
    void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException;


    /**
     * Sauvegarde une nouvelle {@link EcritureComptable ecriture comptable}.
     *
     * @param pEcritureComptable - {@link EcritureComptable l'ecriture comptable} a sauvegarder.
     *
     * @throws FunctionalException Si {@link EcritureComptable l'ecriture comptable} ne respecte pas les regles de gestion
     */
    void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException;


    /**
     * Met a jour {@link EcritureComptable l'ecriture comptable}.
     *
     * @param pEcritureComptable - {@link EcritureComptable L'ecriture comptable} a mettre a jour.
     *
     * @throws FunctionalException Si {@link EcritureComptable l'ecriture comptable} ne respecte pas les regles de gestion
     */
    void updateEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException;

    /**
     * Supprime {@link EcritureComptable l'ecriture comptable} d'id {@code pId}.
     *
     * @param pId - l'id de {@link EcritureComptable l'ecriture}
     */
    void deleteEcritureComptable(Integer pId);
}
