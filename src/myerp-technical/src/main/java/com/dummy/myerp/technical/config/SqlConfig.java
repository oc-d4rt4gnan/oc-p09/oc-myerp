package com.dummy.myerp.technical.config;

import com.dummy.myerp.technical.util.exception.TechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Classe de configuration des requetes SQL depuis le fichier sqlContext.properties
 */
public class SqlConfig {

    /*
     * =================================================================================================================
     *                                             Attributs | Getters | Setters
     * =================================================================================================================
     */

    // Logger Log4j pour la classe
    private Logger logger = LogManager.getLogger(SqlConfig.class);



    // ============================================ SQLSelectListCompteComptable =======================================

    private String sqlSelectListCompteComptable;

    /**
     * Recupere la requete SQL de recuperation d'une liste de compte comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectListCompteComptable() {
        return sqlSelectListCompteComptable;
    }

    private void setSQLSelectListCompteComptable(String sqlSelectListCompteComptable) {
        this.sqlSelectListCompteComptable = sqlSelectListCompteComptable;
    }



    // ========================================= SQLSelectListJournalComptable =========================================

    private String sqlSelectListJournalComptable;

    /**
     * Recupere la requete SQL de recuperation d'une liste de journal comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectListJournalComptable() {
        return sqlSelectListJournalComptable;
    }

    private void setSQLSelectListJournalComptable(String sqlSelectListJournalComptable) {
        this.sqlSelectListJournalComptable = sqlSelectListJournalComptable;
    }



    // ========================================= SQLSelectListEcritureComptable ========================================

    private String sqlSelectListEcritureComptable;

    /**
     * Recupere la requete SQL de recuperation d'une liste de ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectListEcritureComptable() {
        return sqlSelectListEcritureComptable;
    }

    private void setSQLSelectListEcritureComptable(String sqlSelectListEcritureComptable) {
        this.sqlSelectListEcritureComptable = sqlSelectListEcritureComptable;
    }



    // =========================================== SQLSelectEcritureComptable ==========================================

    private String sqlSelectEcritureComptable;

    /**
     * Recupere la requete SQL de recuperation d'une ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectEcritureComptable() {
        return sqlSelectEcritureComptable;
    }

    private void setSQLSelectEcritureComptable(String sqlSelectEcritureComptable) {
        this.sqlSelectEcritureComptable = sqlSelectEcritureComptable;
    }



    // ======================================== SQLSelectEcritureComptableByRef ========================================

    private String sqlSelectEcritureComptableByRef;

    /**
     * Recupere la requete SQL de recuperation d'une ecriture comptable par sa reference
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectEcritureComptableByRef() {
        return sqlSelectEcritureComptableByRef;
    }

    private void setSQLSelectEcritureComptableByRef(String sqlSelectEcritureComptableByRef) {
        this.sqlSelectEcritureComptableByRef = sqlSelectEcritureComptableByRef;
    }



    // ============================================ SQLLoadListLigneEcriture ===========================================

    private String sqlLoadListLigneEcriture;

    /**
     * Recupere la requete SQL de recuperation d'une liste de ligne d'ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLLoadListLigneEcriture() {
        return sqlLoadListLigneEcriture;
    }

    private void setSQLLoadListLigneEcriture(String sqlLoadListLigneEcriture) {
        this.sqlLoadListLigneEcriture = sqlLoadListLigneEcriture;
    }



    // =================== SQLSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year ===================

    private String sqlSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year;

    /**
     * Recupere la requete SQL de recuperation d'une sequence d'ecriture comptable par le code du journal comptable
     * associe et par l'annee de l'ecriture comptable associe
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year() {
        return sqlSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year;
    }

    private void setSQLSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year(
            String sqlSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year) {
        this.sqlSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year =
                sqlSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year;
    }



    // ========================================== SQLInsertEcritureComptable ===========================================

    private String sqlInsertEcritureComptable;

    /**
     * Recupere la requete SQL de sauvegarde d'une ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLInsertEcritureComptable() {
        return sqlInsertEcritureComptable;
    }

    private void setSQLInsertEcritureComptable(String sqlInsertEcritureComptable) {
        this.sqlInsertEcritureComptable = sqlInsertEcritureComptable;
    }



    // ====================================== SQLInsertListLigneEcritureComptable ======================================

    private String sqlInsertListLigneEcritureComptable;

    /**
     * Recupere la requete SQL de sauvegarde d'une liste de ligne d'ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLInsertListLigneEcritureComptable() {
        return sqlInsertListLigneEcritureComptable;
    }

    private void setSQLInsertListLigneEcritureComptable(String sqlInsertListLigneEcritureComptable) {
        this.sqlInsertListLigneEcritureComptable = sqlInsertListLigneEcritureComptable;
    }



    // ======================================= SQLInsertSequenceEcritureComptable ======================================

    private String sqlInsertSequenceEcritureComptable;

    /**
     * Recupere la requete SQL de sauvegarde d'une sequence d'ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLInsertSequenceEcritureComptable() {
        return sqlInsertSequenceEcritureComptable;
    }

    private void setSQLInsertSequenceEcritureComptable(String sqlInsertSequenceEcritureComptable) {
        this.sqlInsertSequenceEcritureComptable = sqlInsertSequenceEcritureComptable;
    }



    // ========================================== SQLInsertJournalComptable ============================================

    private String sqlInsertJournalComptable;

    /**
     * Recupere la requete SQL de sauvegarde d'un journal comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLInsertJournalComptable() {
        return sqlInsertJournalComptable;
    }

    private void setSQLInsertJournalComptable(String sqlInsertJournalComptable) {
        this.sqlInsertJournalComptable = sqlInsertJournalComptable;
    }



    // ========================================== SQLInsertJournalComptable ============================================

    private String sqlInsertCompteComptable;

    /**
     * Recupere la requete SQL de sauvegarde d'un compte comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLInsertCompteComptable() {
        return sqlInsertCompteComptable;
    }

    private void setSQLInsertCompteComptable(String sqlInsertCompteComptable) {
        this.sqlInsertCompteComptable = sqlInsertCompteComptable;
    }



    // =========================================== SQLUpdateEcritureComptable ==========================================

    private String sqlUpdateEcritureComptable;

    /**
     * Recupere la requete SQL de mise a jour d'une ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLUpdateEcritureComptable() {
        return sqlUpdateEcritureComptable;
    }

    private void setSQLUpdateEcritureComptable(String sqlUpdateEcritureComptable) {
        this.sqlUpdateEcritureComptable = sqlUpdateEcritureComptable;
    }



    // ====================================== SQLUpdateSequenceEcritureComptable =======================================

    private String sqlUpdateSequenceEcritureComptable;

    /**
     * Recupere la requete SQL de mise a jour d'une sequence d'ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLUpdateSequenceEcritureComptable() {
        return sqlUpdateSequenceEcritureComptable;
    }

    private void setSQLUpdateSequenceEcritureComptable(String sqlUpdateSequenceEcritureComptable) {
        this.sqlUpdateSequenceEcritureComptable = sqlUpdateSequenceEcritureComptable;
    }



    // =========================================== SQLUpdateJournalComptable ===========================================

    private String sqlUpdateJournalComptable;

    /**
     * Recupere la requete SQL de mise a jour d'un journal comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLUpdateJournalComptable() {
        return sqlUpdateJournalComptable;
    }

    private void setSQLUpdateJournalComptable(String sqlUpdateJournalComptable) {
        this.sqlUpdateJournalComptable = sqlUpdateJournalComptable;
    }



    // =========================================== SQLUpdateCompteComptable ============================================

    private String sqlUpdateCompteComptable;

    /**
     * Recupere la requete SQL de mise a jour d'un compte comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLUpdateCompteComptable() {
        return sqlUpdateCompteComptable;
    }

    private void setSQLUpdateCompteComptable(String sqlUpdateCompteComptable) {
        this.sqlUpdateCompteComptable = sqlUpdateCompteComptable;
    }



    // =========================================== SQLDeleteEcritureComptable ==========================================

    private String sqlDeleteEcritureComptable;

    /**
     * Recupere la requete SQL de suppression d'une ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteEcritureComptable() {
        return sqlDeleteEcritureComptable;
    }

    private void setSQLDeleteEcritureComptable(String sqlDeleteEcritureComptable) {
        this.sqlDeleteEcritureComptable = sqlDeleteEcritureComptable;
    }



    // ====================================== SQLDeleteListLigneEcritureComptable ======================================

    private String sqlDeleteListLigneEcritureComptable;

    /**
     * Recupere la requete SQL de suppression d'une liste de ligne d'ecriture comptable
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteListLigneEcritureComptable() {
        return sqlDeleteListLigneEcritureComptable;
    }

    private void setSQLDeleteListLigneEcritureComptable(String sqlDeleteListLigneEcritureComptable) {
        this.sqlDeleteListLigneEcritureComptable = sqlDeleteListLigneEcritureComptable;
    }



    // ====================================== SQLDeleteAllLigneEcritureComptable =======================================

    private String sqlDeleteAllLigneEcritureComptable;

    /**
     * Recupere la requete SQL de suppression de toutes les lignes d'ecriture comptable de la base de donnees
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteAllLigneEcritureComptable() {
        return sqlDeleteAllLigneEcritureComptable;
    }

    private void setSQLDeleteAllLigneEcritureComptable(String sqlDeleteAllLigneEcritureComptable) {
        this.sqlDeleteAllLigneEcritureComptable = sqlDeleteAllLigneEcritureComptable;
    }



    // ===================================== SQLDeleteAllSequenceEcritureComptable =====================================

    private String sqlDeleteAllSequenceEcritureComptable;

    /**
     * Recupere la requete SQL de suppression de toutes les sequences d'ecriture comptable de la base de donnees
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteAllSequenceEcritureComptable() {
        return sqlDeleteAllSequenceEcritureComptable;
    }

    private void setSQLDeleteAllSequenceEcritureComptable(String sqlDeleteAllSequenceEcritureComptable) {
        this.sqlDeleteAllSequenceEcritureComptable = sqlDeleteAllSequenceEcritureComptable;
    }



    // ========================================= SQLDeleteAllCompteComptable ===========================================

    private String sqlDeleteAllCompteComptable;

    /**
     * Recupere la requete SQL de suppression de tous les comptes comptable de la base de donnees
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteAllCompteComptable() {
        return sqlDeleteAllCompteComptable;
    }

    private void setSQLDeleteAllCompteComptable(String sqlDeleteAllCompteComptable) {
        this.sqlDeleteAllCompteComptable = sqlDeleteAllCompteComptable;
    }



    // ======================================== SQLDeleteAllEcritureComptable ==========================================

    private String sqlDeleteAllEcritureComptable;

    /**
     * Recupere la requete SQL de suppression de toutes les ecritures comptable de la base de donnees
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteAllEcritureComptable() {
        return sqlDeleteAllEcritureComptable;
    }

    private void setSQLDeleteAllEcritureComptable(String sqlDeleteAllEcritureComptable) {
        this.sqlDeleteAllEcritureComptable = sqlDeleteAllEcritureComptable;
    }



    // ========================================= SQLDeleteAllJournalComptable ==========================================

    private String sqlDeleteAllJournalComptable;

    /**
     * Recupere la requete SQL de suppression de tous les journaux comptable de la base de donnees
     *
     * @return La requete sous forme de chaine de caracteres
     */
    public String getSQLDeleteAllJournalComptable() {
        return sqlDeleteAllJournalComptable;
    }

    private void setSQLDeleteAllJournalComptable(String sqlDeleteAllJournalComptable) {
        this.sqlDeleteAllJournalComptable = sqlDeleteAllJournalComptable;
    }



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Instancie les differentes requetes lors de son appel
     */
    public SqlConfig() throws TechnicalException {
        this.setPropValues();
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    private InputStream inputStream;
    private void setPropValues() throws TechnicalException {
        try {
            Properties prop = new Properties();
            String propFileName = "sqlContext.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // Recupere les valeurs depuis le fichier de propriete et les injectes dans les differents attributs de la
            // classe
            // SELECT
            this.setSQLSelectListCompteComptable(prop.getProperty("sql.select.list.compte_comptable"));
            this.setSQLSelectListJournalComptable(prop.getProperty("sql.select.list.journal_comptable"));
            this.setSQLSelectListEcritureComptable(prop.getProperty("sql.select.list.ecriture_comptable"));
            this.setSQLSelectEcritureComptable(prop.getProperty("sql.select.ecriture_comptable"));
            this.setSQLSelectEcritureComptableByRef(prop.getProperty("sql.select.ecriture_comptable.by.ref"));
            this.setSQLLoadListLigneEcriture(prop.getProperty("sql.select.list.ligne_ecriture_comptable"));
            this.setSQLSelectSequenceEcritureComptableByJournalComptable_CodeAndEcriture_Year(prop
                    .getProperty("sql.select.sequence_ecriture_comptable.by.journal_comptable_code.and.ecriture_year"));

            // INSERT
            this.setSQLInsertEcritureComptable(prop.getProperty("sql.insert.ecriture_comptable"));
            this.setSQLInsertListLigneEcritureComptable(prop.getProperty("sql.insert.list.ligne_ecriture_comptable"));
            this.setSQLInsertSequenceEcritureComptable(prop.getProperty("sql.insert.sequence_ecriture_comptable"));
            this.setSQLInsertJournalComptable(prop.getProperty("sql.insert.journal_comptable"));
            this.setSQLInsertCompteComptable(prop.getProperty("sql.insert.compte_comptable"));

            // UPDATE
            this.setSQLUpdateEcritureComptable(prop.getProperty("sql.update.ecriture_comptable"));
            this.setSQLUpdateSequenceEcritureComptable(prop.getProperty("sql.update.sequence_ecriture_comptable"));
            this.setSQLUpdateJournalComptable(prop.getProperty("sql.update.journal_comptable"));
            this.setSQLUpdateCompteComptable(prop.getProperty("sql.update.compte_comptable"));

            // DELETE
            this.setSQLDeleteEcritureComptable(prop.getProperty("sql.delete.ecriture_comptable"));
            this.setSQLDeleteListLigneEcritureComptable(prop.getProperty("sql.delete.list.ligne_ecriture_comptable"));
            this.setSQLDeleteAllLigneEcritureComptable(prop.getProperty("sql.delete.all.ligne_ecriture_comptable"));
            this.setSQLDeleteAllSequenceEcritureComptable(prop.getProperty("sql.delete.all.sequence_ecriture_comptable"));
            this.setSQLDeleteAllCompteComptable(prop.getProperty("sql.delete.all.compte_comptable"));
            this.setSQLDeleteAllEcritureComptable(prop.getProperty("sql.delete.all.ecriture_comptable"));
            this.setSQLDeleteAllJournalComptable(prop.getProperty("sql.delete.all.journal_comptable"));

        } catch (Exception e) {
            throw new TechnicalException("FileNotFound", e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                logger.error("Erreur à la fermeture du fichier sqlContext.properties : {}", e.getMessage());
            }
        }
    }
}
