package com.dummy.myerp.technical.util.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Classe des Exceptions Fonctionnelles (correspond a une erreur 400)
 */
public class FunctionalException extends Exception {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Numero de version
    private static final long serialVersionUID = 1L;

    // Logger Log4j pour la classe
    private final transient Logger logger = LogManager.getLogger(FunctionalException.class);

    // Code d'erreur
    private String vErrorCode;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Creer une exception fonctionnelle avec un code et un message.
     *
     * @param pErrorCode - Le code d'erreur specifique.
     * @param pMessage - Le message d'erreur associe.
     */
    public FunctionalException(String pErrorCode, String pMessage) {
        super(pMessage);
        this.vErrorCode = pErrorCode;
        logger.error(pMessage);
    }

    /**
     * Creer une exception fonctionnelle avec un code, un message et la cause associe.
     *
     * @param pErrorCode - Le code d'erreur specifique.
     * @param pMessage - Le message d'erreur associe.
     * @param pCause - La cause de l'exception.
     */
    public FunctionalException(String pErrorCode, String pMessage, Throwable pCause) {
        super(pMessage, pCause);
        this.vErrorCode = pErrorCode;
        logger.error(pMessage);
    }
}
