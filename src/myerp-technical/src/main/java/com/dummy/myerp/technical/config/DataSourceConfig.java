package com.dummy.myerp.technical.config;

import com.dummy.myerp.technical.util.exception.TechnicalException;
import org.apache.commons.dbcp2.BasicDataSourceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.*;
import java.util.Properties;

/**
 * Classe de configuration de la {@link javax.sql.DataSource}
 */
public class DataSourceConfig {

    /*
     * =================================================================================================================
     *                                                   Attributs
     * =================================================================================================================
     */

    // La DataSource a configurer
    private DataSource dataSource;

    // Logger Log4j pour la classe
    private Logger logger = LogManager.getLogger(DataSourceConfig.class);



    /*
     * =================================================================================================================
     *                                                 Constructeurs
     * =================================================================================================================
     */

    /**
     * Creer et configure une instance de la classe
     *
     * @throws TechnicalException si un probleme survient lors de la lecture ou la fermeture du fichier
     * de configuration
     */
    public DataSourceConfig() throws TechnicalException {
        this.setPropValues();
    }



    /*
     * =================================================================================================================
     *                                              Getters | Setters
     * =================================================================================================================
     */

    /**
     * Renvoie la {@link DataSource} configuree.
     *
     * @return {@link DataSource}
     */
    public DataSource getDataSource() {
        return dataSource;
    }



    /*
     * =================================================================================================================
     *                                                   Methodes
     * =================================================================================================================
     */

    private InputStream inputStream;
    private void setPropValues() throws TechnicalException {
        Properties dataSourceProperties = new Properties();

        // Chargement des proprietes de la datasource depuis le fichier de propriete
        try {
            String propFileName = "datasource.properties";
            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                dataSourceProperties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            logger.error("Fichier non trouvé : {}", e.getMessage());
            throw new TechnicalException("FileNotFound", e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                logger.error("Erreur sur la fermeture du fichier datasource.properties : {}", e.getMessage());
            }
        }

        // Creation de la datasource a partir des proprietes recuperees
        try {
            this.dataSource = BasicDataSourceFactory.createDataSource(dataSourceProperties);
            logger.info("DataSource chargée !");
        } catch (Exception e) {
            logger.error("Erreur lors de la création de la datasource : {}", e.getMessage());
            throw new TechnicalException("DatasourceCreationError", String.format("Erreur lors de la création de la datasource : %s", e.getMessage()));
        }
    }

}
