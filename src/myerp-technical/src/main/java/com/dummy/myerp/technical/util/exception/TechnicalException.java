package com.dummy.myerp.technical.util.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Classe des Exceptions Techniques (correspond a une erreur 500)
 */
public class TechnicalException extends Exception {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Numero de version
    private static final long serialVersionUID = 1L;

    // Logger Log4j pour la classe
    private final transient Logger logger = LogManager.getLogger(TechnicalException.class);

    // Code d'erreur
    private String vErrorCode;



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Creer une exception technique avec un code et un message.
     *
     * @param pErrorCode - Le code d'erreur specifique.
     * @param pMessage - Le message d'erreur associe.
     */
    public TechnicalException(String pErrorCode, String pMessage) {
        super(pMessage);
        this.vErrorCode = pErrorCode;
        logger.error(pMessage);
    }
}
