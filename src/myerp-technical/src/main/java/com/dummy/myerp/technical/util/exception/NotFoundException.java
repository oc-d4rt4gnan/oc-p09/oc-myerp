package com.dummy.myerp.technical.util.exception;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Classe des Exception de type "Donnee non trouvee" (correspond a une erreur 404)
 */
public class NotFoundException extends Exception {

    /*
     * =================================================================================================================
     *                                                     Attributs
     * =================================================================================================================
     */

    // Numero de version
    private static final long serialVersionUID = 1L;

    // Logger Log4j pour la classe
    private final transient Logger logger = LogManager.getLogger(NotFoundException.class);



    /*
     * =================================================================================================================
     *                                                    Constructeurs
     * =================================================================================================================
     */

    /**
     * Creer une exception 404 avec un message.
     *
     * @param pMessage - Le message d'erreur.
     */
    public NotFoundException(String pMessage) {
        super(pMessage);
        logger.error(pMessage);
    }

}
